package com.ruoyi.system.entity;

import com.ruoyi.system.domain.HiasEventClerk;
import lombok.Data;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;
import org.springframework.data.neo4j.core.schema.Relationship;

import java.util.List;
@Data
@Node
public class Event {
    @Id
    private Long eventId;

    @Property
    private String eventName;

    @Relationship(type = "DIRECTED", direction = Relationship.Direction.INCOMING)
    private List<Clerk> eventClerkList;
}
