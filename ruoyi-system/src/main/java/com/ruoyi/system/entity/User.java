package com.ruoyi.system.entity;

import lombok.Data;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;
import org.springframework.data.neo4j.core.schema.Relationship;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Node ("用户")
public class User {

    @Id
    private String nid;
    @Property
    private String name;

    @Relationship(type = "购买", direction = Relationship.Direction.OUTGOING)
    private List<Clothes> buy = new ArrayList<>();

    public User(String name) {
        this.nid = UUID.randomUUID().toString();
        this.name = name;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBuy(List<Clothes> buy) {
        this.buy = buy;
    }

    public String getNid() {
        return nid;
    }

    public String getName() {
        return name;
    }
}
