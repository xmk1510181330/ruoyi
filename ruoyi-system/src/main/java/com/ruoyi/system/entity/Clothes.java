package com.ruoyi.system.entity;

import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;

import java.util.UUID;

@Node
public class Clothes {

    @Id
    private String nid;

    @Property
    private String name;

    public Clothes(String name) {
        this.nid = UUID.randomUUID().toString();
        this.name = name;
    }

    public String getNid() {
        return nid;
    }

    public String getName() {
        return name;
    }
}
