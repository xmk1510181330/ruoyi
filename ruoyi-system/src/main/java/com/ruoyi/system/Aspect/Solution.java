package com.ruoyi.system.Aspect;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Solution {
//    public int majorityElement(int[] nums) {
//        Map<Integer, Integer> counts = new HashMap<Integer, Integer>();
//        for (int num : nums) {
//            if (!counts.containsKey(num)) {
//                counts.put(num, 1);
//            } else {
//                counts.put(num, counts.get(num) + 1);
//            }
//        }
//
//        Set<Integer> set = counts.keySet();
//        for (Integer key : set) {
//            if (counts.get(key) > nums.length/2) {
//                return key;
//            }
//        }
//        return 0;
//    }

    public int majorityElement(int[] nums) {
        int result = nums[0];
        int s = 0;
        for (int i = 0; i < nums.length; i++) {
            if(s == 0) {
                result = nums[i];
            }

            if(nums[i] == result) {
                s++;
            }else {
                s--;
            }
        }
        return result;
    }
}
