package com.ruoyi.system.Aspect;

import com.google.gson.Gson;
import com.ruoyi.system.RSAUtils;
import com.ruoyi.system.domain.bo.HiasActivationSafeBo;
import com.ruoyi.system.domain.bo.HiasActiveCheckBo;
import com.ruoyi.system.domain.bo.HiasActiveCheckSafeBo;
import com.ruoyi.system.domain.to.HiasActivationSafeTo;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;


import java.security.interfaces.RSAPrivateKey;


@Aspect
@Component
public class RSAAspect {

    /** 切入点表达式 */
    @Pointcut("execution(com.ruoyi.system.domain.to.HiasActivationSafeTo com.ruoyi.system.service.IHiasActivationService+.*(com.ruoyi.system.domain.bo.HiasActivationSafeBo))")
    public void rsaPointCut(){}


    @Around("rsaPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        System.out.println(point.toString());;
        Object[] args = point.getArgs();
        HiasActivationSafeBo safeBo = (HiasActivationSafeBo) args[0];
        String privateKeyStr = RSAUtils.readPrivateKey();
        RSAPrivateKey privateKey = RSAUtils.getPrivateKey(privateKeyStr);
        String content = RSAUtils.privateDecrypt(safeBo.getSafeStr(), privateKey);
        safeBo.setSafeStr(content);
        args[0] = safeBo;

        Object result = point.proceed(args);

        HiasActivationSafeTo safeTo = (HiasActivationSafeTo) result;
        String toStr = safeTo.getSafeStr();
        String content2 = RSAUtils.privateEncrypt(toStr, privateKey);
        safeTo.setSafeStr(content2);
        return safeTo;
    }
}
