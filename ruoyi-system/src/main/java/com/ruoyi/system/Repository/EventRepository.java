package com.ruoyi.system.Repository;

import com.ruoyi.system.domain.to.HiasEventHotTO;
import com.ruoyi.system.entity.Event;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends CrudRepository<Event, Long> {

    @Query("match(targetClerk:Clerk{clerkId: $clerkId})-->(targetEvent:Event{eventId: $eventId})<--(otherClerk:Clerk)\n" +
        "match(otherClerk)-->(otherEvent:Event)\n" +
        "where otherEvent.eventId<>$eventId\n" +
        "return otherEvent.eventId as eventId,count(otherEvent) as sum order by sum desc limit 10;")
    List<HiasEventHotTO> hot(@Param("clerkId") Long clerkId, @Param("eventId") Long eventId);

    @Query("create (targetClerk:Clerk{clerkId: $clerkId})-[r:DIRECTED]->(targetEvent:Event{eventId: $eventId});")
    void addRelation(@Param("clerkId") Long clerkId,@Param("eventId") Long eventId);
}
