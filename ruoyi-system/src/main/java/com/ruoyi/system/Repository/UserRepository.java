package com.ruoyi.system.Repository;

import com.ruoyi.system.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
    List<User> findAllByName(String name);

    User findByNid(String nid);
}
