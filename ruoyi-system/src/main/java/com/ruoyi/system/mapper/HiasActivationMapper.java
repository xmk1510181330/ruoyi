package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.HiasActivation;
import com.ruoyi.system.domain.vo.HiasActivationVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2024-03-10
 */
public interface HiasActivationMapper extends BaseMapperPlus<HiasActivationMapper, HiasActivation, HiasActivationVo> {

}
