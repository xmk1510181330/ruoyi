package com.ruoyi.system.mapper;

import com.ruoyi.common.core.mapper.BaseMapperPlus;
import com.ruoyi.system.domain.HiasInnovationPark;
import com.ruoyi.system.domain.HiasParkEnterprise;
import com.ruoyi.system.domain.vo.HiasInnovationParkVo;
import com.ruoyi.system.domain.vo.HiasParkEnterpriseVo;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface HiasParkEnterpriseMapper extends BaseMapperPlus<HiasParkEnterpriseMapper, HiasParkEnterprise, HiasParkEnterpriseVo> {

}
