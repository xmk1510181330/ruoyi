package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.HiasActivationTeam;
import com.ruoyi.system.domain.vo.HiasActivationTeamVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2024-03-09
 */
public interface HiasActivationTeamMapper extends BaseMapperPlus<HiasActivationTeamMapper, HiasActivationTeam, HiasActivationTeamVo> {

}
