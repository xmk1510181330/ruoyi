package com.ruoyi.system.mapper;

import com.ruoyi.common.core.mapper.BaseMapperPlus;
import com.ruoyi.system.domain.HiasEventClerk;
import com.ruoyi.system.domain.HiasEventTag;
import com.ruoyi.system.domain.vo.HiasEventClerkVo;
import com.ruoyi.system.domain.vo.HiasEventTagVo;
import com.ruoyi.system.domain.vo.HiasEventVo;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface HiasEventClerkMapper extends BaseMapperPlus<HiasEventClerkMapper, HiasEventClerk, HiasEventClerkVo> {

}
