package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.HiasInnovationPark;
import com.ruoyi.system.domain.vo.HiasInnovationParkVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface HiasInnovationParkMapper extends BaseMapperPlus<HiasInnovationParkMapper, HiasInnovationPark, HiasInnovationParkVo> {

}
