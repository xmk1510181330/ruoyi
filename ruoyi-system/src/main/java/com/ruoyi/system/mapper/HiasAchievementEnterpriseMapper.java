package com.ruoyi.system.mapper;

import com.ruoyi.common.core.mapper.BaseMapperPlus;
import com.ruoyi.system.domain.HiasAchievementEnterprise;
import com.ruoyi.system.domain.HiasEventClerk;
import com.ruoyi.system.domain.vo.HiasAchievementEnterpriseVo;
import com.ruoyi.system.domain.vo.HiasEventClerkVo;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface HiasAchievementEnterpriseMapper extends BaseMapperPlus<HiasAchievementEnterpriseMapper, HiasAchievementEnterprise, HiasAchievementEnterpriseVo> {

}
