package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.HiasScientificResearchAchievement;
import com.ruoyi.system.domain.vo.HiasScientificResearchAchievementVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface HiasScientificResearchAchievementMapper extends BaseMapperPlus<HiasScientificResearchAchievementMapper, HiasScientificResearchAchievement, HiasScientificResearchAchievementVo> {

}
