package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.HiasEnterprise;
import com.ruoyi.system.domain.vo.HiasEnterpriseVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface HiasEnterpriseMapper extends BaseMapperPlus<HiasEnterpriseMapper, HiasEnterprise, HiasEnterpriseVo> {

}
