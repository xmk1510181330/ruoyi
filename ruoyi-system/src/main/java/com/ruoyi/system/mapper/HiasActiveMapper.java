package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.HiasActive;
import com.ruoyi.system.domain.vo.HiasActiveVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 激活码Mapper接口
 *
 * @author ruoyi
 * @date 2024-03-09
 */
public interface HiasActiveMapper extends BaseMapperPlus<HiasActiveMapper, HiasActive, HiasActiveVo> {

}
