package com.ruoyi.system.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Value("${hias.queue.info}")
    private String infoQueueName;

    @Value("${hias.queue.mail}")
    private String mailQueueName;

    @Value("${hias.queue.wx}")
    private String wxQueueName;


    @Value("${hias.exchange.topicExchange}")
    private String exchangeName;

    @Bean
    public Queue infoQueue() {
        return new Queue(infoQueueName, true);
    }

    @Bean
    public Queue mailQueue() {
        return new Queue(mailQueueName, true);
    }

    @Bean
    public Queue wxQueue() {
        return new Queue(wxQueueName, true);
    }

    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange(exchangeName, true, false);
    }

    // 绑定infoWarnQueue到交换机，处理'hias.info'和'hias.warn'消息
    @Bean
    Binding bindInfoQueue(TopicExchange topicExchange, @Qualifier("infoQueue") Queue q1) {
        return BindingBuilder.bind(q1).to(topicExchange).with("*.info");
    }

    @Bean
    Binding bindMailQueue(TopicExchange topicExchange, @Qualifier("mailQueue") Queue q1) {
        return BindingBuilder.bind(q1).to(topicExchange).with("*.mail");
    }

    @Bean
    Binding bindWxQueue(TopicExchange topicExchange, @Qualifier("wxQueue") Queue q1) {
        return BindingBuilder.bind(q1).to(topicExchange).with("*.wx");
    }
}
