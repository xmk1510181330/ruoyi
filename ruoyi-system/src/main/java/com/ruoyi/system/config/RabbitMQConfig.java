package com.ruoyi.system.config;

import org.aspectj.lang.annotation.Before;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Bean
    public Queue queue() {
        return new Queue("注册激活请求队列", true);
    }
}
