package com.ruoyi.system.message.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.system.domain.HiasClerk;
import com.ruoyi.system.domain.HiasEventClerk;
import com.ruoyi.system.mapper.HiasClerkMapper;
import com.ruoyi.system.mapper.HiasEventClerkMapper;
import com.ruoyi.system.mapper.HiasEventMapper;
import com.ruoyi.system.message.HiasEventBaseMessage;
import lombok.Data;
import org.springframework.data.neo4j.repository.query.Query;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class HiasEventDelayMessage extends HiasEventBaseMessage {

    private String msg;

    @Override
    public void doBuildMessage(HiasEventMapper eventMapper, HiasEventClerkMapper eventClerkMapper, HiasClerkMapper clerkMapper) {
        Map<String, String> target = new HashMap<>();

        QueryWrapper<HiasEventClerk> wrapper = new QueryWrapper<>();
        wrapper.eq("event_id", this.getEventID());
        List<HiasEventClerk> eventClerkList = eventClerkMapper.selectList(wrapper);

        for(HiasEventClerk eventClerk : eventClerkList){
            HiasClerk clerk = clerkMapper.selectById(eventClerk.getClerkId());
            if(clerk != null){
                String key = null;
                if("mail".equals(this.getForm())){
                    key = clerk.getMail();
                }else if("message".equals(this.getForm())){
                    key = clerk.getPhone();
                }
                target.put(key, this.getMsg());
            }
        }

        this.setSendList(target);
    }

//    private Date predictDate;

}
