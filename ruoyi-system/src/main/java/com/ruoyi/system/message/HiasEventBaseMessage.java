package com.ruoyi.system.message;

import com.google.gson.Gson;
import com.ruoyi.system.mapper.HiasClerkMapper;
import com.ruoyi.system.mapper.HiasEventClerkMapper;
import com.ruoyi.system.mapper.HiasEventMapper;
import com.ruoyi.system.message.impl.*;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 消息队列通知的基类
 */
@Data
public abstract class HiasEventBaseMessage {

    static Map<String, Class<? extends HiasEventBaseMessage>> map = new HashMap<>();

    static Map<String, HiasEventSend> sendMap = new HashMap<>();

    static {
        map.put("1", HiasEventCancelMessage.class);
        map.put("2", HiasEventDelayMessage.class);
        map.put("3", HiasEventInfoMessage.class);
        map.put("4", HiasEventQuotaExpansionMessage.class);

        sendMap.put("mail", new HiasEventMailSend());
        sendMap.put("message", new HiasEventMessgeSend());
    }

    Map<String, String> sendList = new HashMap<>();
    /**
     * 消息的ID
     */
    private String messageID;

    /**
     * 活动的ID
     */
    private String eventID;

    /**
     * 消息通知的类型
     */
    private String type;

    private String form;

    public abstract void doBuildMessage(HiasEventMapper baseMapper, HiasEventClerkMapper eventClerkMapper, HiasClerkMapper clerkMapper);

    public void doPost(HiasEventSend send) {
        send.doSend(this.sendList);
    }

    public String toJsonString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static HiasEventBaseMessage toMessage(String content) {
        Gson gson = new Gson();
        //HiasEventBaseMessage baseMessage = gson.fromJson(content, HiasEventBaseMessage.class);
        HashMap<String, String> jsonMap = gson.fromJson(content, HashMap.class);
        String mType = jsonMap.get("type");

        return gson.fromJson(content, map.get(mType));
    }

    public void doLogic(HiasEventMapper baseMapper, HiasEventClerkMapper eventClerkMapper, HiasClerkMapper clerkMapper) {
        doBuildMessage(baseMapper, eventClerkMapper, clerkMapper);
        HiasEventSend sender = sendMap.get(this.getForm());
        doPost(sender);
    }
}
