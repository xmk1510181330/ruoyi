package com.ruoyi.system.message.impl;

import com.ruoyi.system.mapper.HiasClerkMapper;
import com.ruoyi.system.mapper.HiasEventClerkMapper;
import com.ruoyi.system.mapper.HiasEventMapper;
import com.ruoyi.system.message.HiasEventBaseMessage;
import lombok.Data;

@Data
public class HiasEventCancelMessage extends HiasEventBaseMessage {

    private String msg;

    private Integer cancelType;

    @Override
    public void doBuildMessage(HiasEventMapper baseMapper, HiasEventClerkMapper eventClerkMapper, HiasClerkMapper clerkMapper) {

    }
}
