package com.ruoyi.system.message.impl;

import com.ruoyi.system.mapper.HiasClerkMapper;
import com.ruoyi.system.mapper.HiasEventClerkMapper;
import com.ruoyi.system.mapper.HiasEventMapper;
import com.ruoyi.system.message.HiasEventBaseMessage;
import lombok.Data;

@Data
public class HiasEventQuotaExpansionMessage extends HiasEventBaseMessage {

    private Integer quota;

    private Integer pos;

    @Override
    public void doBuildMessage(HiasEventMapper baseMapper, HiasEventClerkMapper eventClerkMapper, HiasClerkMapper clerkMapper) {

    }
}
