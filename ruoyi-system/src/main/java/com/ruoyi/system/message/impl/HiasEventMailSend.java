package com.ruoyi.system.message.impl;

import com.ruoyi.common.utils.email.MailUtils;
import com.ruoyi.system.message.HiasEventSend;

import java.util.Map;
import java.util.Set;

/**
 * The Art of Coding
 */
public class HiasEventMailSend implements HiasEventSend {

    @Override
    public void doSend(Map<String, String> sendList) {
        Set<String> keySet = sendList.keySet();
        for(String key : keySet) {
            if(key != null) {
                MailUtils.sendText(key, "科创大脑活动服务通知", sendList.get(key));
            }
        }
    }
}
