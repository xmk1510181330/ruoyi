package com.ruoyi.system.message;

import java.util.Map;

public interface HiasEventSend {

    void doSend(Map<String, String> sendList);
}
