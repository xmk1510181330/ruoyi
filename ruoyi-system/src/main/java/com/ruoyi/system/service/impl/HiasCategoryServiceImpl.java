package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.HiasCategoryBo;
import com.ruoyi.system.domain.vo.HiasCategoryVo;
import com.ruoyi.system.domain.HiasCategory;
import com.ruoyi.system.mapper.HiasCategoryMapper;
import com.ruoyi.system.service.IHiasCategoryService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@RequiredArgsConstructor
@Service
public class HiasCategoryServiceImpl implements IHiasCategoryService {

    private final HiasCategoryMapper baseMapper;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public HiasCategoryVo queryById(String name){
        return baseMapper.selectVoById(name);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<HiasCategoryVo> queryPageList(HiasCategoryBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<HiasCategory> lqw = buildQueryWrapper(bo);
        Page<HiasCategoryVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<HiasCategoryVo> queryList(HiasCategoryBo bo) {
        LambdaQueryWrapper<HiasCategory> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<HiasCategory> buildQueryWrapper(HiasCategoryBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<HiasCategory> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), HiasCategory::getName, bo.getName());
        lqw.eq(bo.getPid() != null, HiasCategory::getPid, bo.getPid());
        lqw.eq(StringUtils.isNotBlank(bo.getWay()), HiasCategory::getWay, bo.getWay());
        lqw.eq(bo.getWeight() != null, HiasCategory::getWeight, bo.getWeight());
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public Boolean insertByBo(HiasCategoryBo bo) {
        HiasCategory add = BeanUtil.toBean(bo, HiasCategory.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setName(add.getName());
        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(HiasCategoryBo bo) {
        HiasCategory update = BeanUtil.toBean(bo, HiasCategory.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(HiasCategory entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
