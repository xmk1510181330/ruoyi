package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.HiasNewsBo;
import com.ruoyi.system.domain.vo.HiasNewsVo;
import com.ruoyi.system.domain.HiasNews;
import com.ruoyi.system.mapper.HiasNewsMapper;
import com.ruoyi.system.service.IHiasNewsService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@RequiredArgsConstructor
@Service
public class HiasNewsServiceImpl implements IHiasNewsService {

    private final HiasNewsMapper baseMapper;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public HiasNewsVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<HiasNewsVo> queryPageList(HiasNewsBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<HiasNews> lqw = buildQueryWrapper(bo);
        Page<HiasNewsVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<HiasNewsVo> queryList(HiasNewsBo bo) {
        LambdaQueryWrapper<HiasNews> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<HiasNews> buildQueryWrapper(HiasNewsBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<HiasNews> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getPoint() != null, HiasNews::getPoint, bo.getPoint());
        lqw.eq(StringUtils.isNotBlank(bo.getTitle()), HiasNews::getTitle, bo.getTitle());
        lqw.eq(StringUtils.isNotBlank(bo.getSource()), HiasNews::getSource, bo.getSource());
        lqw.eq(bo.getNewsTime() != null, HiasNews::getNewsTime, bo.getNewsTime());
        lqw.eq(StringUtils.isNotBlank(bo.getContent()), HiasNews::getContent, bo.getContent());
        lqw.eq(StringUtils.isNotBlank(bo.getJournalist()), HiasNews::getJournalist, bo.getJournalist());
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public Boolean insertByBo(HiasNewsBo bo) {
        HiasNews add = BeanUtil.toBean(bo, HiasNews.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(HiasNewsBo bo) {
        HiasNews update = BeanUtil.toBean(bo, HiasNews.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(HiasNews entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
