package com.ruoyi.system.service;

import com.ruoyi.system.domain.HiasTeamClerk;
import com.ruoyi.system.domain.vo.HiasTeamClerkVo;
import com.ruoyi.system.domain.bo.HiasTeamClerkBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IHiasTeamClerkService {

    /**
     * 查询【请填写功能名称】
     */
    HiasTeamClerkVo queryById(Long teamId);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasTeamClerkVo> queryPageList(HiasTeamClerkBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasTeamClerkVo> queryList(HiasTeamClerkBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasTeamClerkBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasTeamClerkBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
