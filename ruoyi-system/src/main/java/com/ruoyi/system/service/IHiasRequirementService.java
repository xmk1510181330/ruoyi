package com.ruoyi.system.service;

import com.ruoyi.system.domain.HiasRequirement;
import com.ruoyi.system.domain.vo.HiasRequirementVo;
import com.ruoyi.system.domain.bo.HiasRequirementBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IHiasRequirementService {

    /**
     * 查询【请填写功能名称】
     */
    HiasRequirementVo queryById(Long id);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasRequirementVo> queryPageList(HiasRequirementBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasRequirementVo> queryList(HiasRequirementBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasRequirementBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasRequirementBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    boolean apply(Long did, Long cid);
}
