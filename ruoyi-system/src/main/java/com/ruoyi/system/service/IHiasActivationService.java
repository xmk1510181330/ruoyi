package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.domain.bo.*;
import com.ruoyi.system.domain.to.HiasActivationSafeTo;
import com.ruoyi.system.domain.vo.HiasActivationVo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * com.ruoyi.system.service.IHiasActivationService
 * @author ruoyi
 * @date 2024-03-09
 */
public interface IHiasActivationService {

    /**
     * 查询【请填写功能名称】
     */
    HiasActivationVo queryById(Long id);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasActivationVo> queryPageList(HiasActivationBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasActivationVo> queryList(HiasActivationBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasActivationBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasActivationBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
    R generateActivationCode(HiasActivationGenBo bo);

    R check(HiasActivationCheckBo bo);

    HiasActivationSafeTo generateDeviceIdSafe(HiasActivationSafeBo safeBo) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException;

    R generateActivationCodeSafe(HiasActivationSafeBo safeBo) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException;

    R checkSafe(HiasActivationSafeBo safeBo) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException;
}
