package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.HiasClassifyBo;
import com.ruoyi.system.domain.vo.HiasClassifyVo;
import com.ruoyi.system.domain.HiasClassify;
import com.ruoyi.system.mapper.HiasClassifyMapper;
import com.ruoyi.system.service.IHiasClassifyService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@RequiredArgsConstructor
@Service
public class HiasClassifyServiceImpl implements IHiasClassifyService {

    private final HiasClassifyMapper baseMapper;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public HiasClassifyVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<HiasClassifyVo> queryPageList(HiasClassifyBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<HiasClassify> lqw = buildQueryWrapper(bo);
        Page<HiasClassifyVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<HiasClassifyVo> queryList(HiasClassifyBo bo) {
        LambdaQueryWrapper<HiasClassify> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<HiasClassify> buildQueryWrapper(HiasClassifyBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<HiasClassify> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getClassificationName()), HiasClassify::getClassificationName, bo.getClassificationName());
        lqw.eq(bo.getPid() != null, HiasClassify::getPid, bo.getPid());
        lqw.eq(StringUtils.isNotBlank(bo.getWay()), HiasClassify::getWay, bo.getWay());
        lqw.eq(bo.getWeight() != null, HiasClassify::getWeight, bo.getWeight());
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public Boolean insertByBo(HiasClassifyBo bo) {
        HiasClassify add = BeanUtil.toBean(bo, HiasClassify.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(HiasClassifyBo bo) {
        HiasClassify update = BeanUtil.toBean(bo, HiasClassify.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(HiasClassify entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
