package com.ruoyi.system.service;

import com.ruoyi.system.domain.HiasClassify;
import com.ruoyi.system.domain.vo.HiasClassifyVo;
import com.ruoyi.system.domain.bo.HiasClassifyBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IHiasClassifyService {

    /**
     * 查询【请填写功能名称】
     */
    HiasClassifyVo queryById(Long id);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasClassifyVo> queryPageList(HiasClassifyBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasClassifyVo> queryList(HiasClassifyBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasClassifyBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasClassifyBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
