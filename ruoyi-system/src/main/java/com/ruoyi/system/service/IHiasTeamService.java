package com.ruoyi.system.service;

import com.ruoyi.system.domain.bo.HiasTeamClerkBo;
import com.ruoyi.system.domain.vo.HiasTeamRecordVo;
import com.ruoyi.system.domain.vo.HiasTeamApplyVo;
import com.ruoyi.system.domain.vo.HiasTeamVo;
import com.ruoyi.system.domain.bo.HiasTeamBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IHiasTeamService {

    /**
     * 查询【请填写功能名称】
     */
    HiasTeamVo queryById(Long teamId);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasTeamVo> queryPageList(HiasTeamBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasTeamVo> queryList(HiasTeamBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasTeamBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasTeamBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);

    boolean join(HiasTeamClerkBo hiasTeamClerkBo);

    HiasTeamApplyVo applyList(Long teamId);

    HiasTeamRecordVo applyRecords(Long clerkId);

    boolean checkApply(HiasTeamClerkBo teamClerkBo);
}
