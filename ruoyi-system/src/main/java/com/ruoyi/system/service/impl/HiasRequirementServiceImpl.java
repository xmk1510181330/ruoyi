package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.system.type.ApplyStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.HiasRequirementBo;
import com.ruoyi.system.domain.vo.HiasRequirementVo;
import com.ruoyi.system.domain.HiasRequirement;
import com.ruoyi.system.mapper.HiasRequirementMapper;
import com.ruoyi.system.service.IHiasRequirementService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@RequiredArgsConstructor
@Service
public class HiasRequirementServiceImpl implements IHiasRequirementService {

    private final HiasRequirementMapper baseMapper;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public HiasRequirementVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<HiasRequirementVo> queryPageList(HiasRequirementBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<HiasRequirement> lqw = buildQueryWrapper(bo);
        Page<HiasRequirementVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<HiasRequirementVo> queryList(HiasRequirementBo bo) {
        LambdaQueryWrapper<HiasRequirement> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<HiasRequirement> buildQueryWrapper(HiasRequirementBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<HiasRequirement> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getDemandSideId() != null, HiasRequirement::getDemandSideId, bo.getDemandSideId());
        lqw.eq(bo.getCompletionSideId() != null, HiasRequirement::getCompletionSideId, bo.getCompletionSideId());
        lqw.eq(StringUtils.isNotBlank(bo.getCategory()), HiasRequirement::getCategory, bo.getCategory());
        lqw.eq(StringUtils.isNotBlank(bo.getIndustry()), HiasRequirement::getIndustry, bo.getIndustry());
        lqw.eq(bo.getExpectedTimeOfCompletion() != null, HiasRequirement::getExpectedTimeOfCompletion, bo.getExpectedTimeOfCompletion());
        lqw.eq(StringUtils.isNotBlank(bo.getTypeOfCooperation()), HiasRequirement::getTypeOfCooperation, bo.getTypeOfCooperation());
        //lqw.eq(bo.ge != null, HiasRequirement::getUnveiled amount, bo.getUnveiled amount());
        lqw.eq(StringUtils.isNotBlank(bo.getDescription()), HiasRequirement::getDescription, bo.getDescription());
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public Boolean insertByBo(HiasRequirementBo bo) {
        HiasRequirement add = BeanUtil.toBean(bo, HiasRequirement.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(HiasRequirementBo bo) {
        HiasRequirement update = BeanUtil.toBean(bo, HiasRequirement.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(HiasRequirement entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public boolean apply(Long did, Long cid) {
        //申请方完成过的需求数
        QueryWrapper<HiasRequirement> wrapper = new QueryWrapper<>();
        wrapper.eq(cid != null, "completion_side_id", cid);
        wrapper.eq("status", ApplyStatus.Completed.getStatus());
        wrapper.eq("del_flag",0);
        Long num = baseMapper.selectCount(wrapper);
        System.out.println(num);
        //查询需求的价值
        HiasRequirement requirement = baseMapper.selectById(did);
        if(num >= 10){
            requirement.setStatus(1);
            baseMapper.updateById(requirement);
            return true;
        }
        else if(num >= 5 && requirement.getUnveiledAmount() <100){
            requirement.setStatus(1);
            baseMapper.updateById(requirement);
            return true;
        }
        else if(num > 0 && requirement.getUnveiledAmount() <10){
            requirement.setStatus(1);
            baseMapper.updateById(requirement);
            return true;
        }
        else {
            return false;
        }
    }
}
