package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.HiasEquipmentBo;
import com.ruoyi.system.domain.vo.HiasEquipmentVo;
import com.ruoyi.system.domain.HiasEquipment;
import com.ruoyi.system.mapper.HiasEquipmentMapper;
import com.ruoyi.system.service.IHiasEquipmentService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@RequiredArgsConstructor
@Service
public class HiasEquipmentServiceImpl implements IHiasEquipmentService {

    private final HiasEquipmentMapper baseMapper;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public HiasEquipmentVo queryById(Long id){
        HiasEquipment equip =  baseMapper.selectById(id);
        if(equip.getNumberOfViews() == null){
            equip.setNumberOfViews((long)1);
        }else{
            Long vNumber = equip.getNumberOfViews() + 1;
            equip.setNumberOfViews(vNumber);
        }
        baseMapper.updateById(equip);
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<HiasEquipmentVo> queryPageList(HiasEquipmentBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<HiasEquipment> lqw = buildQueryWrapper(bo);
        Page<HiasEquipmentVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<HiasEquipmentVo> queryList(HiasEquipmentBo bo) {
        LambdaQueryWrapper<HiasEquipment> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<HiasEquipment> buildQueryWrapper(HiasEquipmentBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<HiasEquipment> lqw = Wrappers.lambdaQuery();
        lqw.like(bo.getName() != null, HiasEquipment::getName, bo.getName());
        lqw.orderByDesc(HiasEquipment::getNumberOfViews);
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public Boolean insertByBo(HiasEquipmentBo bo) {
        HiasEquipment add = BeanUtil.toBean(bo, HiasEquipment.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(HiasEquipmentBo bo) {
        HiasEquipment update = BeanUtil.toBean(bo, HiasEquipment.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(HiasEquipment entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
