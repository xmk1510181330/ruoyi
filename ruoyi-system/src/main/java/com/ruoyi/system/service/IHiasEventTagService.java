package com.ruoyi.system.service;

import com.ruoyi.system.domain.HiasEventTag;
import com.ruoyi.system.domain.vo.HiasEventTagVo;
import com.ruoyi.system.domain.bo.HiasEventTagBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IHiasEventTagService {

    /**
     * 查询【请填写功能名称】
     */
    HiasEventTagVo queryById(Long eventId);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasEventTagVo> queryPageList(HiasEventTagBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasEventTagVo> queryList(HiasEventTagBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasEventTagBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasEventTagBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
