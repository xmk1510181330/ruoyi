package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.HiasTeamClerkBo;
import com.ruoyi.system.domain.vo.HiasTeamClerkVo;
import com.ruoyi.system.domain.HiasTeamClerk;
import com.ruoyi.system.mapper.HiasTeamClerkMapper;
import com.ruoyi.system.service.IHiasTeamClerkService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@RequiredArgsConstructor
@Service
public class HiasTeamClerkServiceImpl implements IHiasTeamClerkService {

    private final HiasTeamClerkMapper baseMapper;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public HiasTeamClerkVo queryById(Long teamId){
        return baseMapper.selectVoById(teamId);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<HiasTeamClerkVo> queryPageList(HiasTeamClerkBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<HiasTeamClerk> lqw = buildQueryWrapper(bo);
        Page<HiasTeamClerkVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<HiasTeamClerkVo> queryList(HiasTeamClerkBo bo) {
        LambdaQueryWrapper<HiasTeamClerk> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<HiasTeamClerk> buildQueryWrapper(HiasTeamClerkBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<HiasTeamClerk> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getTeamId() != null, HiasTeamClerk::getTeamId, bo.getTeamId());
        lqw.eq(bo.getClerkId() != null, HiasTeamClerk::getClerkId, bo.getClerkId());
        lqw.eq(bo.getWorkMode() != null, HiasTeamClerk::getWorkMode, bo.getWorkMode());
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public Boolean insertByBo(HiasTeamClerkBo bo) {
        HiasTeamClerk add = BeanUtil.toBean(bo, HiasTeamClerk.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setTeamId(add.getTeamId());
        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(HiasTeamClerkBo bo) {
        HiasTeamClerk update = BeanUtil.toBean(bo, HiasTeamClerk.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(HiasTeamClerk entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
