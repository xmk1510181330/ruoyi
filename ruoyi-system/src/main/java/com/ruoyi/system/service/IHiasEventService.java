package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.domain.HiasEventClerk;
import com.ruoyi.system.domain.bo.HiasEventClerkBo;
import com.ruoyi.system.domain.bo.HiasEventDelayBo;
import com.ruoyi.system.domain.vo.HiasEventHotVo;
import com.ruoyi.system.domain.vo.HiasEventVo;
import com.ruoyi.system.domain.bo.HiasEventBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;


import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IHiasEventService {

    /**
     * 查询【请填写功能名称】
     */
    HiasEventVo queryById(Long id, Long clerkId);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasEventVo> queryPageList(HiasEventBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasEventVo> queryList(HiasEventBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasEventBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasEventBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    String join(HiasEventClerkBo bo);

    List<HiasEventClerk> applylist(Long eventId);

    String checkApplication(Long id, int checkStaus);

    Map<Long, String> checkError();


    Boolean load();

    List<HiasEventHotVo> hot(Long clerkId, Long eventId);

    R eventDelay(HiasEventDelayBo bo);
}
