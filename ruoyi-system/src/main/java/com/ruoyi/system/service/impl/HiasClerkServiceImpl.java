package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.HiasClerkBo;
import com.ruoyi.system.domain.vo.HiasClerkVo;
import com.ruoyi.system.domain.HiasClerk;
import com.ruoyi.system.mapper.HiasClerkMapper;
import com.ruoyi.system.service.IHiasClerkService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@RequiredArgsConstructor
@Service
public class HiasClerkServiceImpl implements IHiasClerkService {

    private final HiasClerkMapper baseMapper;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public HiasClerkVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<HiasClerkVo> queryPageList(HiasClerkBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<HiasClerk> lqw = buildQueryWrapper(bo);
        Page<HiasClerkVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<HiasClerkVo> queryList(HiasClerkBo bo) {
        LambdaQueryWrapper<HiasClerk> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<HiasClerk> buildQueryWrapper(HiasClerkBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<HiasClerk> lqw = Wrappers.lambdaQuery();
        //lqw.like(StringUtils.isNotBlank(bo.g), HiasClerk::getCName, bo.getCName());
        lqw.eq(bo.getGender() != null, HiasClerk::getGender, bo.getGender());
        lqw.eq(StringUtils.isNotBlank(bo.getMail()), HiasClerk::getMail, bo.getMail());
        lqw.eq(StringUtils.isNotBlank(bo.getNo()), HiasClerk::getNo, bo.getNo());
        lqw.eq(StringUtils.isNotBlank(bo.getPhone()), HiasClerk::getPhone, bo.getPhone());
        lqw.eq(StringUtils.isNotBlank(bo.getIntro()), HiasClerk::getIntro, bo.getIntro());
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public Boolean insertByBo(HiasClerkBo bo) {
        HiasClerk add = BeanUtil.toBean(bo, HiasClerk.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(HiasClerkBo bo) {
        HiasClerk update = BeanUtil.toBean(bo, HiasClerk.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(HiasClerk entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
