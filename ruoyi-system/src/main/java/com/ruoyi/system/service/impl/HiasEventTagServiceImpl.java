package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.HiasEventTagBo;
import com.ruoyi.system.domain.vo.HiasEventTagVo;
import com.ruoyi.system.domain.HiasEventTag;
import com.ruoyi.system.mapper.HiasEventTagMapper;
import com.ruoyi.system.service.IHiasEventTagService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@RequiredArgsConstructor
@Service
public class HiasEventTagServiceImpl implements IHiasEventTagService {

    private final HiasEventTagMapper baseMapper;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public HiasEventTagVo queryById(Long eventId){
        return baseMapper.selectVoById(eventId);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<HiasEventTagVo> queryPageList(HiasEventTagBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<HiasEventTag> lqw = buildQueryWrapper(bo);
        Page<HiasEventTagVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<HiasEventTagVo> queryList(HiasEventTagBo bo) {
        LambdaQueryWrapper<HiasEventTag> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<HiasEventTag> buildQueryWrapper(HiasEventTagBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<HiasEventTag> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getEventId() != null, HiasEventTag::getEventId, bo.getEventId());
        lqw.eq(bo.getTagId() != null, HiasEventTag::getTagId, bo.getTagId());
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public Boolean insertByBo(HiasEventTagBo bo) {
        HiasEventTag add = BeanUtil.toBean(bo, HiasEventTag.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setEventId(add.getEventId());
        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(HiasEventTagBo bo) {
        HiasEventTag update = BeanUtil.toBean(bo, HiasEventTag.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(HiasEventTag entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
