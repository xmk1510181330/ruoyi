package com.ruoyi.system.service;

import com.ruoyi.system.domain.HiasEquipment;
import com.ruoyi.system.domain.vo.HiasEquipmentVo;
import com.ruoyi.system.domain.bo.HiasEquipmentBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IHiasEquipmentService {

    /**
     * 查询【请填写功能名称】
     */
    HiasEquipmentVo queryById(Long id);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasEquipmentVo> queryPageList(HiasEquipmentBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasEquipmentVo> queryList(HiasEquipmentBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasEquipmentBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasEquipmentBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
