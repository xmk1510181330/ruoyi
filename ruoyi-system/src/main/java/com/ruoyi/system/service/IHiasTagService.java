package com.ruoyi.system.service;

import com.ruoyi.system.domain.HiasTag;
import com.ruoyi.system.domain.vo.HiasTagVo;
import com.ruoyi.system.domain.bo.HiasTagBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IHiasTagService {

    /**
     * 查询【请填写功能名称】
     */
    HiasTagVo queryById(Long id);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasTagVo> queryPageList(HiasTagBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasTagVo> queryList(HiasTagBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasTagBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasTagBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
