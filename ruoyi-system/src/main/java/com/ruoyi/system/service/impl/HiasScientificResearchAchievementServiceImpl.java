package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.system.domain.HiasAchievementEnterprise;
import com.ruoyi.system.domain.bo.HiasAchievementEnterpriseBo;
import com.ruoyi.system.mapper.HiasAchievementEnterpriseMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.HiasScientificResearchAchievementBo;
import com.ruoyi.system.domain.vo.HiasScientificResearchAchievementVo;
import com.ruoyi.system.domain.HiasScientificResearchAchievement;
import com.ruoyi.system.mapper.HiasScientificResearchAchievementMapper;
import com.ruoyi.system.service.IHiasScientificResearchAchievementService;

import java.util.List;
import java.util.Map;
import java.util.Collection;
import java.util.Queue;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@RequiredArgsConstructor
@Service
public class HiasScientificResearchAchievementServiceImpl implements IHiasScientificResearchAchievementService {

    private final HiasScientificResearchAchievementMapper baseMapper;
    private final HiasAchievementEnterpriseMapper achievementEnterpriseMapper;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public HiasScientificResearchAchievementVo queryById(Long id){
        HiasScientificResearchAchievement sci = baseMapper.selectById(id);
        if (sci.getViewNumber() == null){
            sci.setViewNumber((long)1);
        }else {
            Long viewNumber = sci.getViewNumber()+1;
            sci.setViewNumber(viewNumber);
        }

        baseMapper.updateById(sci);
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<HiasScientificResearchAchievementVo> queryPageList(HiasScientificResearchAchievementBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<HiasScientificResearchAchievement> lqw = buildQueryWrapper(bo);
        Page<HiasScientificResearchAchievementVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<HiasScientificResearchAchievementVo> queryList(HiasScientificResearchAchievementBo bo) {
        LambdaQueryWrapper<HiasScientificResearchAchievement> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<HiasScientificResearchAchievement> buildQueryWrapper(HiasScientificResearchAchievementBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<HiasScientificResearchAchievement> lqw = Wrappers.lambdaQuery();
        lqw.like(bo.getName() != null, HiasScientificResearchAchievement::getName, bo.getName());
        lqw.orderByDesc(HiasScientificResearchAchievement::getViewNumber);
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public Boolean insertByBo(HiasScientificResearchAchievementBo bo) {
        HiasScientificResearchAchievement add = BeanUtil.toBean(bo, HiasScientificResearchAchievement.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(HiasScientificResearchAchievementBo bo) {
        HiasScientificResearchAchievement update = BeanUtil.toBean(bo, HiasScientificResearchAchievement.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(HiasScientificResearchAchievement entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public String join(HiasAchievementEnterpriseBo bo) {
        //科研成果是否存在
        QueryWrapper<HiasScientificResearchAchievement> wrapper = new QueryWrapper<>();
        wrapper.eq(bo.getAchievementId() != null, "id", bo.getAchievementId());
        boolean achievementExist = baseMapper.exists(wrapper);
        if(achievementExist){
            //是否申请过
            QueryWrapper<HiasAchievementEnterprise> wrapper1 = new QueryWrapper<>();
            wrapper1.eq(bo.getAchievementId() != null, "achievement_id", bo.getAchievementId());
            wrapper1.eq(bo.getApplyId() != null, "apply_id", bo.getApplyId());
            wrapper1.eq(bo.getCheckStatus()>=0 && bo.getCheckStatus()<3, "check_status", 1);
            boolean joinExist = achievementEnterpriseMapper.exists(wrapper1);

            if(joinExist){
                return "已经申请过！";
            }else{
                HiasAchievementEnterprise hiasAchievementEnterprise = new HiasAchievementEnterprise();
                hiasAchievementEnterprise.setAchievementId(bo.getAchievementId());
                hiasAchievementEnterprise.setApplyId(bo.getApplyId());
                hiasAchievementEnterprise.setAffiliatedUnitId(bo.getAffiliatedUnitId());
                hiasAchievementEnterprise.setCheckStatus(0);
                achievementEnterpriseMapper.insert(hiasAchievementEnterprise);
                return "申请成功！";
            }

        }else{
            return "科研成果不存在！";
        }

    }
}
