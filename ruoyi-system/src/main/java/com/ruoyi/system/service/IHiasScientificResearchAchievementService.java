package com.ruoyi.system.service;

import com.ruoyi.system.domain.HiasScientificResearchAchievement;
import com.ruoyi.system.domain.bo.HiasAchievementEnterpriseBo;
import com.ruoyi.system.domain.vo.HiasScientificResearchAchievementVo;
import com.ruoyi.system.domain.bo.HiasScientificResearchAchievementBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IHiasScientificResearchAchievementService {

    /**
     * 查询【请填写功能名称】
     */
    HiasScientificResearchAchievementVo queryById(Long id);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasScientificResearchAchievementVo> queryPageList(HiasScientificResearchAchievementBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasScientificResearchAchievementVo> queryList(HiasScientificResearchAchievementBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasScientificResearchAchievementBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasScientificResearchAchievementBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    String join(HiasAchievementEnterpriseBo bo);
}
