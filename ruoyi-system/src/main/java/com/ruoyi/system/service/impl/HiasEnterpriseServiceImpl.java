package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.vo.HiasEnterpriseDetailVo;
import com.ruoyi.system.mapper.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.HiasEnterpriseBo;
import com.ruoyi.system.domain.vo.HiasEnterpriseVo;
import com.ruoyi.system.service.IHiasEnterpriseService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@RequiredArgsConstructor
@Service
public class HiasEnterpriseServiceImpl implements IHiasEnterpriseService {

    private final HiasEnterpriseMapper baseMapper;
    private final HiasCategoryMapper categoryBaseMapper;
    private final HiasEquipmentMapper equipmentMapper;
    private final HiasScientificResearchAchievementMapper achievementMapper;
    private final HiasAchievementEnterpriseMapper achievementEnterpriseMapper;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public HiasEnterpriseDetailVo queryById(Long id){
        HiasEnterpriseDetailVo result = new HiasEnterpriseDetailVo();
        HiasEnterprise enterprise = baseMapper.selectById(id);
        result.setEnterprise(enterprise);
        //查科研成果
        QueryWrapper<HiasAchievementEnterprise> wrapper = new QueryWrapper<>();
        wrapper.eq("affiliated_unit_id",id);
        List<HiasAchievementEnterprise> achievementEnterprises = achievementEnterpriseMapper.selectList(wrapper);
        List<HiasScientificResearchAchievement> achievements = new ArrayList<>();
        for (int i = 0; i < achievementEnterprises.size(); i++) {
           achievements.add(achievementMapper.selectById(achievementEnterprises.get(i).getAchievementId()));
        }
        result.setAchievements(achievements);
        //TODO 科研设备的加入

        //点击量+1
        if(enterprise.getViewNumber() == null){
            enterprise.setViewNumber((long)1);
        }else{
            enterprise.setViewNumber(enterprise.getViewNumber() + 1);
        }
        baseMapper.updateById(enterprise);
        return result;
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<HiasEnterpriseVo> queryPageList(HiasEnterpriseBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<HiasEnterprise> lqw = buildQueryWrapper(bo);
        Page<HiasEnterpriseVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<HiasEnterpriseVo> queryList(HiasEnterpriseBo bo) {
        LambdaQueryWrapper<HiasEnterprise> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<HiasEnterprise> buildQueryWrapper(HiasEnterpriseBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<HiasEnterprise> lqw = Wrappers.lambdaQuery();
        lqw.like(bo.getName() != null, HiasEnterprise::getName, bo.getName());
        lqw.orderByDesc(HiasEnterprise::getViewNumber);
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public Boolean insertByBo(HiasEnterpriseBo bo) {
        HiasEnterprise add = BeanUtil.toBean(bo, HiasEnterprise.class);
        validEntityBeforeSave(add);
        add.setViewNumber((long)0);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(HiasEnterpriseBo bo) {
        HiasEnterprise update = BeanUtil.toBean(bo, HiasEnterprise.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(HiasEnterprise entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public List<HiasEnterprise> selectByName(String name) {
        QueryWrapper<HiasEnterprise> wrapper = new QueryWrapper<>();
        wrapper.like("name",name);
        return baseMapper.selectList(wrapper);

    }

    @Override
    public List<HiasEnterprise> selectByCategory(int cid) {
        QueryWrapper<HiasCategory> wrapper = new QueryWrapper<>();
        wrapper.like("way","-"+cid+"-");
//        List<> categoryBaseMapper.selectList(wrapper);
         List<HiasCategory> categoryList = categoryBaseMapper.selectList(wrapper);
         List<Long> cidList = new ArrayList<>();
        for (int i = 0; i < categoryList.size(); i++) {
            cidList.add(categoryList.get(i).getId());
        }
        cidList.add((long)cid);
         QueryWrapper<HiasEnterprise> wrapper1 = new QueryWrapper<>();
         wrapper1.in(cidList.size()>0,"category",cidList);
         return baseMapper.selectList(wrapper1);
    }
}
