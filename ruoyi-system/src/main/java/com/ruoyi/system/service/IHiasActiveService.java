package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.domain.HiasActive;
import com.ruoyi.system.domain.bo.HiasActiveCheckBo;
import com.ruoyi.system.domain.bo.HiasActiveCheckSafeBo;
import com.ruoyi.system.domain.bo.HiasActiveGenBo;
import com.ruoyi.system.domain.vo.HiasActiveVo;
import com.ruoyi.system.domain.bo.HiasActiveBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Collection;
import java.util.List;

/**
 * 激活码Service接口
 *
 * @author ruoyi
 * @date 2024-03-09
 */
public interface IHiasActiveService {

    /**
     * 查询激活码
     */
    HiasActiveVo queryById(Long id);

    /**
     * 查询激活码列表
     */
    TableDataInfo<HiasActiveVo> queryPageList(HiasActiveBo bo, PageQuery pageQuery);

    /**
     * 查询激活码列表
     */
    List<HiasActiveVo> queryList(HiasActiveBo bo);

    /**
     * 新增激活码
     */
    Boolean insertByBo(HiasActiveBo bo);

    /**
     * 修改激活码
     */
    Boolean updateByBo(HiasActiveBo bo);

    /**
     * 校验并批量删除激活码信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    R generateActiveCode(HiasActiveGenBo genBo);

    R activeCheck(HiasActiveCheckBo activeCheckBo);

    R activeCheckSafe(HiasActiveCheckSafeBo safeBo) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException;
}
