package com.ruoyi.system.service;

import com.ruoyi.system.domain.HiasClerk;
import com.ruoyi.system.domain.vo.HiasClerkVo;
import com.ruoyi.system.domain.bo.HiasClerkBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IHiasClerkService {

    /**
     * 查询【请填写功能名称】
     */
    HiasClerkVo queryById(Long id);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasClerkVo> queryPageList(HiasClerkBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasClerkVo> queryList(HiasClerkBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasClerkBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasClerkBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
