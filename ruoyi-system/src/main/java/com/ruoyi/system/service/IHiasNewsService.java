package com.ruoyi.system.service;

import com.ruoyi.system.domain.HiasNews;
import com.ruoyi.system.domain.vo.HiasNewsVo;
import com.ruoyi.system.domain.bo.HiasNewsBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IHiasNewsService {

    /**
     * 查询【请填写功能名称】
     */
    HiasNewsVo queryById(Long id);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasNewsVo> queryPageList(HiasNewsBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasNewsVo> queryList(HiasNewsBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasNewsBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasNewsBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
