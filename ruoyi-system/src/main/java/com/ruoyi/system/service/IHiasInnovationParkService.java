package com.ruoyi.system.service;

import com.ruoyi.system.domain.HiasEnterprise;
import com.ruoyi.system.domain.HiasInnovationPark;
import com.ruoyi.system.domain.vo.HiasInnovationParkVo;
import com.ruoyi.system.domain.bo.HiasInnovationParkBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IHiasInnovationParkService {

    /**
     * 查询【请填写功能名称】
     */
    HiasInnovationParkVo queryById(Long id);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasInnovationParkVo> queryPageList(HiasInnovationParkBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasInnovationParkVo> queryList(HiasInnovationParkBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasInnovationParkBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasInnovationParkBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    List<HiasEnterprise> selectEnterpriseByParkId(Long parkId);
}
