package com.ruoyi.system.service;

import com.ruoyi.system.domain.HiasCategory;
import com.ruoyi.system.domain.vo.HiasCategoryVo;
import com.ruoyi.system.domain.bo.HiasCategoryBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IHiasCategoryService {

    /**
     * 查询【请填写功能名称】
     */
    HiasCategoryVo queryById(String name);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasCategoryVo> queryPageList(HiasCategoryBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasCategoryVo> queryList(HiasCategoryBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasCategoryBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasCategoryBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);
}
