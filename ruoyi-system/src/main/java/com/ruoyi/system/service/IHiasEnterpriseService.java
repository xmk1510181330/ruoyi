package com.ruoyi.system.service;

import com.ruoyi.system.domain.HiasEnterprise;
import com.ruoyi.system.domain.vo.HiasEnterpriseDetailVo;
import com.ruoyi.system.domain.vo.HiasEnterpriseVo;
import com.ruoyi.system.domain.bo.HiasEnterpriseBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IHiasEnterpriseService {

    /**
     * 查询【请填写功能名称】
     */
    HiasEnterpriseDetailVo queryById(Long id);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<HiasEnterpriseVo> queryPageList(HiasEnterpriseBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<HiasEnterpriseVo> queryList(HiasEnterpriseBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(HiasEnterpriseBo bo);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(HiasEnterpriseBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    List<HiasEnterprise> selectByName(String name);

    List<HiasEnterprise> selectByCategory(int cid);
}
