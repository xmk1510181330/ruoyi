package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.system.domain.HiasEnterprise;
import com.ruoyi.system.domain.HiasParkEnterprise;
import com.ruoyi.system.mapper.HiasEnterpriseMapper;
import com.ruoyi.system.mapper.HiasParkEnterpriseMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.HiasInnovationParkBo;
import com.ruoyi.system.domain.vo.HiasInnovationParkVo;
import com.ruoyi.system.domain.HiasInnovationPark;
import com.ruoyi.system.mapper.HiasInnovationParkMapper;
import com.ruoyi.system.service.IHiasInnovationParkService;

import java.util.*;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@RequiredArgsConstructor
@Service
public class HiasInnovationParkServiceImpl implements IHiasInnovationParkService {

    private final HiasInnovationParkMapper baseMapper;
    private final HiasParkEnterpriseMapper parkEnterpriseMapper;
    private final HiasEnterpriseMapper enterpriseMapper;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public HiasInnovationParkVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<HiasInnovationParkVo> queryPageList(HiasInnovationParkBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<HiasInnovationPark> lqw = buildQueryWrapper(bo);
        Page<HiasInnovationParkVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<HiasInnovationParkVo> queryList(HiasInnovationParkBo bo) {
        LambdaQueryWrapper<HiasInnovationPark> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<HiasInnovationPark> buildQueryWrapper(HiasInnovationParkBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<HiasInnovationPark> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getAffiliatedUnitId() != null, HiasInnovationPark::getAffiliatedUnitId, bo.getAffiliatedUnitId());
        lqw.eq(StringUtils.isNotBlank(bo.getCategory()), HiasInnovationPark::getCategory, bo.getCategory());
        lqw.eq(StringUtils.isNotBlank(bo.getLocalArea()), HiasInnovationPark::getLocalArea, bo.getLocalArea());
        lqw.eq(StringUtils.isNotBlank(bo.getLevel()), HiasInnovationPark::getLevel, bo.getLevel());
        lqw.eq(bo.getArea() != null, HiasInnovationPark::getArea, bo.getArea());
        lqw.eq(StringUtils.isNotBlank(bo.getAddress()), HiasInnovationPark::getAddress, bo.getAddress());
        lqw.eq(StringUtils.isNotBlank(bo.getDescription()), HiasInnovationPark::getDescription, bo.getDescription());
        lqw.eq(StringUtils.isNotBlank(bo.getIndustry()), HiasInnovationPark::getIndustry, bo.getIndustry());
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public Boolean insertByBo(HiasInnovationParkBo bo) {
        HiasInnovationPark add = BeanUtil.toBean(bo, HiasInnovationPark.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(HiasInnovationParkBo bo) {
        HiasInnovationPark update = BeanUtil.toBean(bo, HiasInnovationPark.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(HiasInnovationPark entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public List<HiasEnterprise> selectEnterpriseByParkId(Long parkId) {
        QueryWrapper<HiasParkEnterprise> wrapper = new QueryWrapper<>();
        wrapper.eq(parkId != null, "park_id", parkId);
        wrapper.eq("check_status", 1);
        Date now = new Date();
        wrapper.lt("start_time", now);
        wrapper.ge("end_time", now);
        List<HiasParkEnterprise> parkEnterprises = parkEnterpriseMapper.selectList(wrapper);

        List<HiasEnterprise> resultList = new ArrayList<>();
        for (int i = 0; i < parkEnterprises.size(); i++) {
//            QueryWrapper<HiasEnterprise> wrapper1 = new QueryWrapper<>();
//            wrapper1.eq("id", parkEnterprises.get(i).getApplyId());
            resultList.add(enterpriseMapper.selectById(parkEnterprises.get(i).getApplyId()));
        }
        return resultList;
    }
}
