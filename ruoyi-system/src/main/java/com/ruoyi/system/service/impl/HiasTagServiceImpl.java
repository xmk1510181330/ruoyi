package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.HiasTagBo;
import com.ruoyi.system.domain.vo.HiasTagVo;
import com.ruoyi.system.domain.HiasTag;
import com.ruoyi.system.mapper.HiasTagMapper;
import com.ruoyi.system.service.IHiasTagService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@RequiredArgsConstructor
@Service
public class HiasTagServiceImpl implements IHiasTagService {

    private final HiasTagMapper baseMapper;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public HiasTagVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<HiasTagVo> queryPageList(HiasTagBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<HiasTag> lqw = buildQueryWrapper(bo);
        Page<HiasTagVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<HiasTagVo> queryList(HiasTagBo bo) {
        LambdaQueryWrapper<HiasTag> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<HiasTag> buildQueryWrapper(HiasTagBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<HiasTag> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getTagName()), HiasTag::getTagName, bo.getTagName());
        lqw.eq(StringUtils.isNotBlank(bo.getTagIntro()), HiasTag::getTagIntro, bo.getTagIntro());
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public Boolean insertByBo(HiasTagBo bo) {
        HiasTag add = BeanUtil.toBean(bo, HiasTag.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(HiasTagBo bo) {
        HiasTag update = BeanUtil.toBean(bo, HiasTag.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(HiasTag entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
