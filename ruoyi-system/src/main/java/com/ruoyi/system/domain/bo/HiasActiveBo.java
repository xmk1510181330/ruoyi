package com.ruoyi.system.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 激活码业务对象 hias_active
 *
 * @author ruoyi
 * @date 2024-03-09
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class HiasActiveBo extends BaseEntity {

    /**
     * 记录ID
     */
    @NotNull(message = "记录ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 激活码
     */
    @NotNull(message = "激活码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String activeCode;

    /**
     * 激活类型 0--个人版本 1-- 企业版本
     */
    @NotNull(message = "激活类型 0--个人版本 1-- 企业版本不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long type;

    /**
     * 激活持续时间，按年记录
     */
    @NotNull(message = "激活持续时间，按年记录不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long activeDuration;

    /**
     * 激活状态 0-创建未使用，1-已绑定，2-已过期失效
     */
    @NotNull(message = "激活状态 0-创建未使用，1-已绑定，2-已过期失效不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long activeState;

    /**
     * 激活设备编码
     */
    @NotBlank(message = "激活设备编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String activeDevice;

    /**
     * 时间序列号
     */
    @NotNull(message = "时间序列号不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long activeSerial;

    /**
     * 激活时间
     */
    @NotNull(message = "激活时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date activeTime;


}
