package com.ruoyi.system.domain.vo;

import lombok.Data;

@Data
public class HiasActivationRegisterVo {
    private String uuid;
    private int length;
}
