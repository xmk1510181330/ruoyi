package com.ruoyi.system.domain.bo;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 【请填写功能名称】业务对象 hias_innovation_park
 *
 * @author ruoyi
 * @date 2024-01-11
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class HiasParkEnterpriseBo extends BaseEntity {

    @NotNull(message = "不能为空", groups = {  EditGroup.class })
    private Long id;
    /**
     *
     */
    @NotNull(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long affiliatedUnitId;
    /**
     *
     */
    @NotNull(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long applyId;
    /**
     *
     */
    @NotNull(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date startTime;
    /**
     *
     */
    @NotNull(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date endTime;
    /**
     *
     */
    @NotNull(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long parkId;
    /**
     *
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private int checkStatus;


}
