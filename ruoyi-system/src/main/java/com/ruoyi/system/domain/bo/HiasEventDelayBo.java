package com.ruoyi.system.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class HiasEventDelayBo {

    @NotNull(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long eventId;

    @NotNull(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date delayStartDate;

    @NotNull(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date delayEndDate;

    @NotBlank(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private String noticeType;
}
