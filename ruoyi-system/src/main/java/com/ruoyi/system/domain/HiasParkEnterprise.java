package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 【请填写功能名称】对象 hias_innovation_park
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@TableName("hias_park_enterprise")
public class HiasParkEnterprise  {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    private Long id;
    /**
     *
     */
    private Long affiliatedUnitId;
    /**
     *
     */
    private Long applyId;
    /**
     *
     */
    private Date startTime;
    /**
     *
     */
    private Date endTime;
    /**
     *
     */
    private Long parkId;
    /**
     *
     */
    private int checkStatus;


}
