package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_innovation_park
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("hias_innovation_park")
public class HiasInnovationPark extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    private Long id;
    /**
     * 
     */
    private Long affiliatedUnitId;
    /**
     * 
     */
    private String category;
    /**
     * 
     */
    private String localArea;
    /**
     * 
     */
    private String level;
    /**
     * 
     */
    private Long area;
    /**
     * 
     */
    private String address;
    /**
     * 
     */
    private String description;
    /**
     * 
     */
    private String industry;
    /**
     * 
     */
    @TableLogic
    private String delFlag;

}
