package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_activation
 *
 * @author ruoyi
 * @date 2024-03-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("hias_activation")
public class HiasActivation extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 
     */
    private Long type;
    /**
     * 
     */
    private String deviceId;
    /**
     * 
     */
    private String code;
    /**
     * 
     */
    private String codeState;
    /**
     * 
     */
    private Date maturityTime;
    /**
     * 
     */
    @TableLogic
    private String delFlag;
    /**
     * 
     */
    private Long length;
    /**
     * 
     */
    @Version
    private String version;
    /**
     * 
     */
    private Long max;

}
