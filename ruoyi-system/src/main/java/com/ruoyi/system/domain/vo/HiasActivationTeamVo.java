
package com.ruoyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
 * 【请填写功能名称】视图对象 hias_activation_team
 *
 * @author ruoyi
 * @date 2024-03-09
 */
@Data
@ExcelIgnoreUnannotated
public class HiasActivationTeamVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long state;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String code;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String deviceId;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long serialNumber;


}
