package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_team
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("hias_team")
public class HiasTeam extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    private String teamName;
    /**
     *
     */
    private Long id;
    /**
     *
     */
    private String teamIntro;
    /**
     *
     */
    private Long organizationId;
    /**
     *
     */
    private String address;
    /**
     *
     */
    private String mail;
    /**
     *
     */
    @TableLogic
    private String delFlag;

}
