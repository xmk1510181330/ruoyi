package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 激活码对象 hias_active
 *
 * @author ruoyi
 * @date 2024-03-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("hias_active")
public class HiasActive extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 记录ID
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 激活码
     */
    private String activeCode;
    /**
     * 激活类型 0--个人版本 1-- 企业版本
     */
    private Long type;
    /**
     * 激活持续时间，按年记录
     */
    private Long activeDuration;
    /**
     * 激活状态 0-创建未使用，1-已绑定，2-已过期失效
     */
    private Long activeState;
    /**
     * 激活设备编码
     */
    private String activeDevice;
    /**
     * 时间序列号
     */
    private Long activeSerial;
    /**
     * 激活时间
     */
    private Date activeTime;
    /**
     *
     */
    @TableLogic
    private String delFlag;

}
