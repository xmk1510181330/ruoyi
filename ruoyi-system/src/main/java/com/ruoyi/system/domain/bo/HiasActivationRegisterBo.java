package com.ruoyi.system.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class HiasActivationRegisterBo {
    @NotBlank(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private String hash;

    @NotBlank(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private String code;
}
