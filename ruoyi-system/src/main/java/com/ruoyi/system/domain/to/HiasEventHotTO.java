package com.ruoyi.system.domain.to;

import lombok.Data;

@Data
public class HiasEventHotTO {
    private Long eventId;
    private int sum;

}
