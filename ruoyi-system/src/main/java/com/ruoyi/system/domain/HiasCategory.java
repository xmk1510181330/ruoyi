package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_category
 *
 * @author ruoyi
 * @date 2024-01-11
 */

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("hias_category")
public class HiasCategory extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    private String name;
    /**
     *
     */
    private Long id;
    /**
     *
     */
    private Long pid;
    /**
     *
     */
    private String way;
    /**
     *
     */
    private Long weight;

}
