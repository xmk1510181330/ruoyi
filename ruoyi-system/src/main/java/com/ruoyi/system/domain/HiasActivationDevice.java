package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_activation_device
 *
 * @author ruoyi
 * @date 2024-03-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("hias_activation_device")
public class HiasActivationDevice extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 
     */
    private Long state;
    /**
     * 
     */
    private String code;
    /**
     * 
     */
    private String deviceId;
    /**
     * 
     */
    private Long serialNumber;
    /**
     * 
     */
    @TableLogic
    private String delFlag;
    /**
     * 
     */
    private String hash;

}
