package com.ruoyi.system.domain;

import java.util.HashMap;

public enum TeamApplyStatus {
    NEW(0, "未审批"),
    PASS(1, "审批通过"),
    FAIL(2, "审批不通过");

    private int code;

    private String intro;

    static HashMap<Integer, String> statusMap;

    static {
        statusMap = new HashMap<>();
        statusMap.put(0, "未审批");
        statusMap.put(1, "审批通过");
        statusMap.put(2, "审批不通过");
    }

    public int getCode() {
        return code;
    }

    public String getIntro() {
        return intro;
    }

    public static String getStatus(int code) {
        return statusMap.get(code);
    }

    TeamApplyStatus(int code, String intro) {
        this.code = code;
        this.intro = intro;
    }
}
