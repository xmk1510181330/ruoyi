package com.ruoyi.system.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
 * 【请填写功能名称】视图对象 hias_activation
 *
 * @author ruoyi
 * @date 2024-03-10
 */
@Data
@ExcelIgnoreUnannotated
public class HiasActivationVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long type;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private String deviceId;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private String code;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private String codeState;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Date maturityTime;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long length;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long max;


}
