package com.ruoyi.system.domain.vo;

import lombok.Data;

import java.util.Date;

@Data
public class HiasTeamApplyClerkVo {

    private Long teamClerkApplyId;
    /**
     * 职员ID
     */
    private Long clerkId;

    /**
     * 职员姓名
     */
    private String clerkName;

    /**
     * 研究方向简介
     */
    private String intro;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 申请时间
     */
    private Date applyTime;

    /**
     * 工作方式
     */
    private String workMode;

    /**
     * 当前审批状态
     */
    private String applyStatus;
}
