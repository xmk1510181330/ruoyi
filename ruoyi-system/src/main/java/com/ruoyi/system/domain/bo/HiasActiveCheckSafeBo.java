package com.ruoyi.system.domain.bo;

import lombok.Data;

@Data
public class HiasActiveCheckSafeBo {

    private String safeStr;
}
