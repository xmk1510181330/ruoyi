package com.ruoyi.system.domain.bo;

import lombok.Data;

@Data
public class HiasActiveGenBo {

    private Long type;

    private Long activeDuration;
}
