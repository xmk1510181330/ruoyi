package com.ruoyi.system.domain.to;

import lombok.Data;

///Users/mingke/Documents/gitlab/ruoyi/ruoyi-system/src/main/java/com/ruoyi/system/domain/to/HiasActivationSafeTo.java
@Data
public class HiasActivationSafeTo {
    private Integer code;
    private String msg;
    private String safeStr;
}
