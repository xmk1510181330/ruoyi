package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_news
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("hias_news")
public class HiasNews extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    private Long id;
    /**
     * 
     */
    private Long point;
    /**
     * 
     */
    private String title;
    /**
     * 
     */
    private String source;
    /**
     * 
     */
    private Date newsTime;
    /**
     * 
     */
    private String content;
    /**
     * 
     */
    private String journalist;
    /**
     * 
     */
    @TableLogic
    private String delFlag;

}
