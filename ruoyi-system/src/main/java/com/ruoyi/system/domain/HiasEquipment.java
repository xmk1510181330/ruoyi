package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_equipment
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("hias_equipment")
public class HiasEquipment extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    private Long id;
    /**
     *
     */
    private Long affiliatedUnitId;
    /**
     *
     */
    private String category;
    /**
     *
     */
    private String manufacturer;
    /**
     *
     */
    private String type;
    /**
     *
     */
    private String workingCondition;
    /**
     *
     */
    private String place;
    /**
     *
     */
    private Date dateOfPurchase;
    /**
     *
     */
    private String intro;
    /**
     *
     */
    private String performanceParameter;
    /**
     *
     */
    private String function;
    /**
     *
     */
    private Long numberOfViews;

    private String name;
    /**
     *
     */
    @TableLogic
    private String delFlag;

}
