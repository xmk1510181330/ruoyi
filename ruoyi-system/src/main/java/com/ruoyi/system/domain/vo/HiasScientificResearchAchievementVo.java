package com.ruoyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
 * 【请填写功能名称】视图对象 hias_scientific_research_achievement
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@ExcelIgnoreUnannotated
public class HiasScientificResearchAchievementVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long affiliatedUnitId;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String category;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String manifestationOfResults;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String industrialDirection;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String phase;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String contactPerson;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String contactInformation;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String industryLabel;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String applicationField;


    @ExcelProperty(value = "")
    private Long viewNumber;

    @ExcelProperty(value = "")
    private Long name;


}
