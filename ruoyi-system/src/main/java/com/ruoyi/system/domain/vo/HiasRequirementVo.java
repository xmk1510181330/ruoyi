package com.ruoyi.system.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
 * 【请填写功能名称】视图对象 hias_requirement
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@ExcelIgnoreUnannotated
public class HiasRequirementVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long demandSideId;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long completionSideId;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String category;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String industry;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Date expectedTimeOfCompletion;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String typeOfCooperation;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long unveiledAmount;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String description;

    @ExcelProperty(value = "")
    private int status;


}
