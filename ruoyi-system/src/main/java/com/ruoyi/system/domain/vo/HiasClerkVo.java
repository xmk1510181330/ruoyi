package com.ruoyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
 * 【请填写功能名称】视图对象 hias_clerk
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@ExcelIgnoreUnannotated
public class HiasClerkVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String clerkName;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long gender;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String mail;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String no;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String phone;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String intro;


}
