package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_event_tag
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@TableName("hias_event_tag")
public class HiasEventTag  {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    private Long eventId;
    /**
     *
     */
    private Long tagId;

}
