package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_enterprise
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("hias_enterprise")
public class HiasEnterprise extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    private Long id;
    /**
     *
     */
    private String status;
    /**
     *
     */
    private String legalPerson;
    /**
     *
     */
    private String registeredCapital;
    /**
     *
     */
    private Date registrationDate;
    /**
     *
     */
    private String name;
    /**
     *
     */
    private String englishName;
    /**
     *
     */
    private String website;
    /**
     *
     */
    private String emailAddress;
    /**
     *
     */
    private String paidInCapital;
    /**
     *
     */
    private String telephone;
    /**
     *
     */
    private Long uniformCreditCode;
    /**
     *
     */
    private Long businessRegistrationNumber;
    /**
     *
     */
    private Long taxpayerIdentificationNumber;
    /**
     *
     */
    private String category;
    /**
     *
     */
    @TableLogic
    private String delFlag;

    private Long viewNumber;

}
