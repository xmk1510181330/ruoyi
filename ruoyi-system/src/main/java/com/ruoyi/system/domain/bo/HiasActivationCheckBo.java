package com.ruoyi.system.domain.bo;

import lombok.Data;

import java.util.Date;

@Data
public class HiasActivationCheckBo {
    private String code;
    private String deviceId;
    private int serialNumber;
    private String hash;
}
