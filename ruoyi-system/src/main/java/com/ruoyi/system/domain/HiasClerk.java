package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_clerk
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("hias_clerk")
public class HiasClerk extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    private Long id;
    /**
     *
     */
    private String clerkName;
    /**
     *
     */
    private Long gender;
    /**
     *
     */
    private String mail;
    /**
     *
     */
    private String no;
    /**
     *
     */
    private String phone;
    /**
     *
     */
    private String intro;
    /**
     *
     */
    @TableLogic
    private String delFlag;

}
