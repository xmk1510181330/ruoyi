package com.ruoyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
 * 【请填写功能名称】视图对象 hias_innovation_park
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@ExcelIgnoreUnannotated
public class HiasInnovationParkVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long affiliatedUnitId;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private String category;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private String localArea;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private String level;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long area;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private String address;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private String description;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private String industry;


}
