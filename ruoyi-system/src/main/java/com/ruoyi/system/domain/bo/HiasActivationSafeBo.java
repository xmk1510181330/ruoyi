package com.ruoyi.system.domain.bo;

import com.ruoyi.system.annotion.HiasRequireDecrypt;
import lombok.Data;

@Data
public class HiasActivationSafeBo {

    @HiasRequireDecrypt(algorithm = "MD5")
    private String safeStr;
}
