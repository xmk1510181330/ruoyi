package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_requirement
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("hias_requirement")
public class HiasRequirement extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    private Long id;
    /**
     *
     */
    private Long demandSideId;
    /**
     *
     */
    private Long completionSideId;
    /**
     *
     */
    private String category;
    /**
     *
     */
    private String industry;
    /**
     *
     */
    private Date expectedTimeOfCompletion;
    /**
     *
     */
    private String typeOfCooperation;
    /**
     *
     */
    private Long unveiledAmount;
    /**
     *
     */
    private String description;
    /**
     *
     */
    @TableLogic
    private String delFlag;

    private int status;

    //List<HiasEnterprise> enterpriseList;
}
