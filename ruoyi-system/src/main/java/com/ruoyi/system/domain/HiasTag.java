package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_tag
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("hias_tag")
public class HiasTag extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    private Long id;
    /**
     * 
     */
    private String tagName;
    /**
     * 
     */
    private String tagIntro;
    /**
     * 
     */
    @TableLogic
    private String delFlag;

}
