package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_event
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("hias_event")
public class HiasEvent extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    private Long id;
    /**
     * 
     */
    private String eventName;
    /**
     * 
     */
    private String location;
    /**
     * 
     */
    private Date startTime;
    /**
     * 
     */
    private Date endTime;
    /**
     * 
     */
    private Date registerDeadline;
    /**
     * 
     */
    private Long max;
    /**
     * 
     */
    private String intro;
    /**
     * 
     */
    private String banner;
    /**
     * 
     */
    private Long masterId;
    /**
     * 
     */
    private Long point;
    /**
     * 
     */
    @TableLogic
    private String delFlag;

}
