package com.ruoyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class HiasEventHotVo {
    @ExcelProperty(value = "")
    private Long id;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String eventName;

    @ExcelProperty(value = "")
    private String banner;
}
