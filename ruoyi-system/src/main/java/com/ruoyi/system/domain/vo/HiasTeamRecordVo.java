package com.ruoyi.system.domain.vo;

import lombok.Data;
import java.util.List;

@Data
public class HiasTeamRecordVo {
    private  Long clerkId;
    private String clerkName;

    List<HiasTeamApplyRecordVo> applyRecordVos;
}
