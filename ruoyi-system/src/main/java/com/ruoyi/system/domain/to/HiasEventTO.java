package com.ruoyi.system.domain.to;

import com.ruoyi.system.domain.HiasEventClerk;
import lombok.Data;

import java.util.List;

@Data
public class HiasEventTO {
    private Long id;
    /**
     *
     */
    private String eventName;

    private List<HiasEventClerk> eventClerkList;

}
