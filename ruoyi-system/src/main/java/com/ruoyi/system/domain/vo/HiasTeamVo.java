package com.ruoyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import com.ruoyi.system.domain.HiasClerk;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;
import java.util.List;

/**
 * 【请填写功能名称】视图对象 hias_team
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@ExcelIgnoreUnannotated
public class HiasTeamVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String teamName;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String teamIntro;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long organizationId;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String address;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String mail;

    private List<HiasTeamApplyClerkVo> clerkList;
}
