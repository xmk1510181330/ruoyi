package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("hias_event_clerk")
public class HiasEventClerk {
    private static final long serialVersionUID=1L;

    /**
     *
     */
    private Long eventId;
    /**
     *
     */
    private Long clerkId;

    private int checkStatus;
    private Long id;

    private Date followTime;

}
