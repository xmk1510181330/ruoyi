package com.ruoyi.system.domain;

import lombok.Data;

@Data
public class HiasAchievementEnterprise {

    private static final long serialVersionUID=1L;
    private Long applyId;
    /**
     *
     */
    private Long achievementId;

    private Long  affiliatedUnitId;
    private Long id;

    private int checkStatus;

}
