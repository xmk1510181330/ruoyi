package com.ruoyi.system.domain;

import java.util.HashMap;

public enum TeamWorkMode {
    Master(1, "硕士研究生"),
    Doctor(2, "博士研究生"),
    Assistant(3, "科研助理"),
    Researcher(4, "特聘研究员"),
    Expert(5, "专家教授");

    private int mode;

    private String name;

    static HashMap<Integer, String> modeMap;

    static {
        modeMap = new HashMap<>();
        modeMap.put(1, "硕士研究生");
        modeMap.put(2, "博士研究生");
        modeMap.put(3, "科研助理");
        modeMap.put(4, "特聘研究员");
        modeMap.put(5, "专家教授");
    }

    public int getMode() {
        return mode;
    }

    public String getName() {
        return name;
    }

    TeamWorkMode(int mode, String name) {
        this.mode = mode;
        this.name = name;
    }

    public static String getWorkMode(Integer code) {
        return modeMap.get(code);
    }

}
