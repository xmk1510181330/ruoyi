package com.ruoyi.system.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
 * 激活码视图对象 hias_active
 *
 * @author ruoyi
 * @date 2024-03-09
 */
@Data
@ExcelIgnoreUnannotated
public class HiasActiveVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 记录ID
     */
    @ExcelProperty(value = "记录ID")
    private Long id;

    /**
     * 激活码
     */
    @ExcelProperty(value = "激活码")
    private String activeCode;

    /**
     * 激活类型 0--个人版本 1-- 企业版本
     */
    @ExcelProperty(value = "激活类型 0--个人版本 1-- 企业版本")
    private Long type;

    /**
     * 激活持续时间，按年记录
     */
    @ExcelProperty(value = "激活持续时间，按年记录")
    private Long activeDuration;

    /**
     * 激活状态 0-创建未使用，1-已绑定，2-已过期失效
     */
    @ExcelProperty(value = "激活状态 0-创建未使用，1-已绑定，2-已过期失效")
    private Long activeState;

    /**
     * 激活设备编码
     */
    @ExcelProperty(value = "激活设备编码")
    private String activeDevice;

    /**
     * 时间序列号
     */
    @ExcelProperty(value = "时间序列号")
    private Long activeSerial;

    /**
     * 激活时间
     */
    @ExcelProperty(value = "激活时间")
    private Date activeTime;


}
