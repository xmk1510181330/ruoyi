package com.ruoyi.system.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class HiasTeamApplyVo {

    private Long teamId;

    private String teamName;

    private Long teamSize;

    List<HiasTeamApplyClerkVo> applyClerkVos;
}
