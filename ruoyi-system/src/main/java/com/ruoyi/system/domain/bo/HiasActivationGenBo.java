package com.ruoyi.system.domain.bo;

import lombok.Data;
import org.springframework.data.neo4j.core.support.DateLong;

import java.util.Date;

@Data
public class HiasActivationGenBo {
    /**
     * 过期时间
     */
    private Date maturityTime;

    /**
     * 步距
     */
    private int length;

    /**
     * 个人 -- 机构
     */
    private Long type;

    /**
     * 软件版本
     */
    private String version;

    /**
     * 最大人数
     */
    private Long max;
}
