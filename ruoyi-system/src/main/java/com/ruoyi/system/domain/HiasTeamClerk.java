package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_team_clerk
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@TableName("hias_team_clerk")
public class HiasTeamClerk {

    private static final long serialVersionUID=1L;

    /**
     * 请注意，普通关联表不需要ID字段，但是如果是需要修改关联信息的表，最好还是设一个ID
     * 方便修改状态
     */
    private Long id;
    /**
     *
     */
    private Long teamId;
    /**
     *
     */
    private Long clerkId;
    /**
     *
     */
    private Integer workMode;

    private Integer applyStatus;

    private String applyMark;

    private Date applyTime;
}
