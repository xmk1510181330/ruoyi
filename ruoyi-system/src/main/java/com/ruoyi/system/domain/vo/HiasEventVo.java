package com.ruoyi.system.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
 * 【请填写功能名称】视图对象 hias_event
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@ExcelIgnoreUnannotated
public class HiasEventVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String eventName;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String location;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Date startTime;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Date endTime;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Date registerDeadline;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long max;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String intro;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String banner;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long masterId;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long point;

    @ExcelProperty(value = "")
    private int uStatus;//0=未报名，1=报名过

}
