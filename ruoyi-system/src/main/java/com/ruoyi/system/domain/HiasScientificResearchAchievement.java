package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hias_scientific_research_achievement
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("hias_scientific_research_achievement")
public class HiasScientificResearchAchievement extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    private Long id;
    /**
     *
     */
    private Long affiliatedUnitId;
    /**
     *
     */
    private String category;
    /**
     *
     */
    private String manifestationOfResults;
    /**
     *
     */
    private String industrialDirection;
    /**
     *
     */
    private String phase;
    /**
     *
     */
    private String contactPerson;
    /**
     *
     */
    private String contactInformation;
    /**
     *
     */
    private String industryLabel;
    /**
     *
     */
    private String applicationField;

    private Long viewNumber;

    private String name;

    /**
     *
     */
    @TableLogic
    private String delFlag;

}
