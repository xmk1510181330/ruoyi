package com.ruoyi.system.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
 * 【请填写功能名称】视图对象 hias_enterprise
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@ExcelIgnoreUnannotated
public class HiasEnterpriseVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String status;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String legalPerson;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String registeredCapital;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Date registrationDate;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String name;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String englishName;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String website;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String emailAddress;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String paidInCapital;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String telephone;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long uniformCreditCode;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long businessRegistrationNumber;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long taxpayerIdentificationNumber;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String category;

    @ExcelProperty(value = "点击数")
    private Long viewNumber;
















}
