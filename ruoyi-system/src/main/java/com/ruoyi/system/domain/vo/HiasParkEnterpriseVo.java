package com.ruoyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 【请填写功能名称】视图对象 hias_innovation_park
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@ExcelIgnoreUnannotated
public class HiasParkEnterpriseVo implements Serializable {

    private static final long serialVersionUID = 1L;



    private Long id;
    /**
     *
     */
    private Long affiliatedUnitId;
    /**
     *
     */
    private Long applyId;
    /**
     *
     */
    private Date startTime;
    /**
     *
     */
    private Date endTime;
    /**
     *
     */
    private Long parkId;
    /**
     *
     */
    private int checkStatus;

    /**
     *
     */


}
