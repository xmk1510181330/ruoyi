package com.ruoyi.system.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
 * 【请填写功能名称】视图对象 hias_equipment
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@ExcelIgnoreUnannotated
public class HiasEquipmentVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long affiliatedUnitId;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String category;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String manufacturer;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String type;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String workingCondition;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String place;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Date dateOfPurchase;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String intro;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String performanceParameter;

    /**
     *
     */
    @ExcelProperty(value = "")
    private String function;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long numberOfViews;


    @ExcelProperty(value = "")
    private String name;


}
