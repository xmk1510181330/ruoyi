package com.ruoyi.system.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
 * 【请填写功能名称】视图对象 hias_news
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@ExcelIgnoreUnannotated
public class HiasNewsVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long point;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private String title;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private String source;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Date newsTime;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private String content;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private String journalist;


}
