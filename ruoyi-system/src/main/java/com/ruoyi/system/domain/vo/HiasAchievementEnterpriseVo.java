package com.ruoyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 【请填写功能名称】视图对象 hias_event_tag
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Data
@ExcelIgnoreUnannotated
public class HiasAchievementEnterpriseVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long applyId;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long achievementId;


    @ExcelProperty(value = "")
    private Long affiliatedUnitId;



    @ExcelProperty(value = "")
    private Long id;

    @ExcelProperty(value = "")
    private int checkStatus;


}
