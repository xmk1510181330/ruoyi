package com.ruoyi.system.domain.vo;

import lombok.Data;

import java.util.Date;

@Data
public class HiasTeamApplyRecordVo {
    private Long teamClerkApplyId;
    private Long teamId;
    private String teamName;
    private Date applyTime;
    private String applyStatus;
    private String applyMark;
}
