package com.ruoyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.system.domain.HiasEnterprise;
import com.ruoyi.system.domain.HiasEquipment;
import com.ruoyi.system.domain.HiasScientificResearchAchievement;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class HiasEnterpriseDetailVo {

    private HiasEnterprise enterprise;

    private List<HiasScientificResearchAchievement> achievements;

    private List<HiasEquipment> equipments;
}
