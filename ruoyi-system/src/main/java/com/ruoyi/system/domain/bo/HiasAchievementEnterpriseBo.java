package com.ruoyi.system.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
public class HiasAchievementEnterpriseBo {

    @NotNull(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long applyId;

    /**
     *
     */
    @NotNull(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long achievementId;


    @NotNull(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long affiliatedUnitId;

    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private int checkStatus;

}
