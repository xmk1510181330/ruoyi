package com.ruoyi.system.domain.bo;


import lombok.Data;

@Data
public class HiasActiveCheckBo {

    private String activeCode;

    private String activeDevice;

    private Long activeSerial;

    private String hash;
}
