package com.ruoyi.system.event;

import org.springframework.context.ApplicationEvent;

import java.time.Clock;

/**
 * MQ通知事件
 */
public class MQNoticeEvent extends ApplicationEvent {

    public MQNoticeEvent(Object source) {
        super(source);
    }

    public MQNoticeEvent(Object source, Clock clock) {
        super(source, clock);
    }
}
