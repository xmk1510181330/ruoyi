package com.ruoyi.system.type;


public enum ApplyStatus {
    NEW(0, "刚创建"),
    NonCheck(1, "已申请"),
    CheckOK(2, "审批通过"),
    CheckFail(3, "审批未通过"),
    Completed(4,"需求完成");


    private Integer status;

    private String desc;

    public Integer getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }

    ApplyStatus(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }
}
