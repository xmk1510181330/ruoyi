package com.ruoyi.system.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import com.ruoyi.system.domain.bo.HiasAchievementEnterpriseBo;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.HiasScientificResearchAchievementVo;
import com.ruoyi.system.domain.bo.HiasScientificResearchAchievementBo;
import com.ruoyi.system.service.IHiasScientificResearchAchievementService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/scientificResearchAchievement")
public class HiasScientificResearchAchievementController extends BaseController {

    private final IHiasScientificResearchAchievementService iHiasScientificResearchAchievementService;

    /**
     * 查询【请填写功能名称】列表
     */
    @SaCheckPermission("system:scientificResearchAchievement:list")
    @GetMapping("/list")
    public TableDataInfo<HiasScientificResearchAchievementVo> list(HiasScientificResearchAchievementBo bo, PageQuery pageQuery) {
        return iHiasScientificResearchAchievementService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @SaCheckPermission("system:scientificResearchAchievement:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HiasScientificResearchAchievementBo bo, HttpServletResponse response) {
        List<HiasScientificResearchAchievementVo> list = iHiasScientificResearchAchievementService.queryList(bo);
        ExcelUtil.exportExcel(list, "【请填写功能名称】", HiasScientificResearchAchievementVo.class, response);
    }

    /**
     * 获取【请填写功能名称】详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:scientificResearchAchievement:query")
    @GetMapping("/{id}")
    public R<HiasScientificResearchAchievementVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iHiasScientificResearchAchievementService.queryById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @SaCheckPermission("system:scientificResearchAchievement:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody HiasScientificResearchAchievementBo bo) {
        return toAjax(iHiasScientificResearchAchievementService.insertByBo(bo));
    }

    /**
     * 修改【请填写功能名称】
     */
    @SaCheckPermission("system:scientificResearchAchievement:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody HiasScientificResearchAchievementBo bo) {
        return toAjax(iHiasScientificResearchAchievementService.updateByBo(bo));
    }

    /**
     * 删除【请填写功能名称】
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:scientificResearchAchievement:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iHiasScientificResearchAchievementService.deleteWithValidByIds(Arrays.asList(ids), true));
    }



    /**
     * @intro 申请科研成果合作
     * @param bo 企业申请科研活动合作的参数
     * @return 申请结果
     */
    @PostMapping("/join")
    public String join(@RequestBody HiasAchievementEnterpriseBo bo) {
        return iHiasScientificResearchAchievementService.join(bo);
    }

//    public applyList(){
//
//    }


}
