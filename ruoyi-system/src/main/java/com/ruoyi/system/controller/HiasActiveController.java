package com.ruoyi.system.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import com.ruoyi.system.domain.bo.HiasActiveCheckBo;
import com.ruoyi.system.domain.bo.HiasActiveCheckSafeBo;
import com.ruoyi.system.domain.bo.HiasActiveGenBo;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.HiasActiveVo;
import com.ruoyi.system.domain.bo.HiasActiveBo;
import com.ruoyi.system.service.IHiasActiveService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 激活码
 *
 * @author ruoyi
 * @date 2024-03-09
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/active")
public class HiasActiveController extends BaseController {

    private final IHiasActiveService iHiasActiveService;

    /**
     * 查询激活码列表
     */
    @SaCheckPermission("system:active:list")
    @GetMapping("/list")
    public TableDataInfo<HiasActiveVo> list(HiasActiveBo bo, PageQuery pageQuery) {
        return iHiasActiveService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出激活码列表
     */
    @SaCheckPermission("system:active:export")
    @Log(title = "激活码", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HiasActiveBo bo, HttpServletResponse response) {
        List<HiasActiveVo> list = iHiasActiveService.queryList(bo);
        ExcelUtil.exportExcel(list, "激活码", HiasActiveVo.class, response);
    }

    /**
     * 获取激活码详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:active:query")
    @GetMapping("/{id}")
    public R<HiasActiveVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iHiasActiveService.queryById(id));
    }

    /**
     * 新增激活码
     */
    @SaCheckPermission("system:active:add")
    @Log(title = "激活码", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody HiasActiveBo bo) {
        return toAjax(iHiasActiveService.insertByBo(bo));
    }

    /**
     * 修改激活码
     */
    @SaCheckPermission("system:active:edit")
    @Log(title = "激活码", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody HiasActiveBo bo) {
        return toAjax(iHiasActiveService.updateByBo(bo));
    }

    /**
     * 删除激活码
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:active:remove")
    @Log(title = "激活码", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iHiasActiveService.deleteWithValidByIds(Arrays.asList(ids), true));
    }

    @PostMapping("gen")
    public R generateActiveCode(@RequestBody HiasActiveGenBo genBo) {
        return iHiasActiveService.generateActiveCode(genBo);
    }

    @PostMapping("generateDeviceID")
    public R generateDeviceID() {
        String uuid = UUID.randomUUID().toString();
        return R.ok(uuid);
    }

    @PostMapping("activeCheck")
    public R activeCheck(@RequestBody HiasActiveCheckBo activeCheckBo) {
        return iHiasActiveService.activeCheck(activeCheckBo);
    }

    //归一化
    @PostMapping("activeCheckSafe")
    public R activeCheckSafe(@RequestBody HiasActiveCheckSafeBo safeStr) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        return iHiasActiveService.activeCheckSafe(safeStr);
    }
}
