package com.ruoyi.system.controller;

import java.util.Date;
import java.util.List;
import java.util.Arrays;
import java.util.Map;

import com.ruoyi.system.domain.HiasEventClerk;
import com.ruoyi.system.domain.bo.HiasEventClerkBo;
import com.ruoyi.system.domain.bo.HiasEventDelayBo;
import com.ruoyi.system.domain.vo.HiasEventHotVo;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.HiasEventVo;
import com.ruoyi.system.domain.bo.HiasEventBo;
import com.ruoyi.system.service.IHiasEventService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/event")
public class HiasEventController extends BaseController {

    private final IHiasEventService iHiasEventService;

    /**
     * 查询【请填写功能名称】列表
     */
    @SaCheckPermission("system:event:list")
    @GetMapping("/list")
    public TableDataInfo<HiasEventVo> list(HiasEventBo bo, PageQuery pageQuery) {
        return iHiasEventService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @SaCheckPermission("system:event:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HiasEventBo bo, HttpServletResponse response) {
        List<HiasEventVo> list = iHiasEventService.queryList(bo);
        ExcelUtil.exportExcel(list, "【请填写功能名称】", HiasEventVo.class, response);
    }

    /**
     * 获取【请填写功能名称】详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:event:query")
    @GetMapping("/{id}")
    public R<HiasEventVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id, Long clerkId) {
        return R.ok(iHiasEventService.queryById(id, clerkId));
    }

    /**
     * 新增【请填写功能名称】
     */
    @SaCheckPermission("system:event:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody HiasEventBo bo) {
        return toAjax(iHiasEventService.insertByBo(bo));
    }

    /**
     * 修改【请填写功能名称】
     */
    @SaCheckPermission("system:event:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody HiasEventBo bo) {
        return toAjax(iHiasEventService.updateByBo(bo));
    }

    /**
     * 删除【请填写功能名称】
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:event:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iHiasEventService.deleteWithValidByIds(Arrays.asList(ids), true));
    }


    /**
     * 用户申请加入活动
     * @param bo 用户加入活动的参数BO
     * @return 加入的结果
     */
    @PostMapping("/join")
    public String join(@Validated(AddGroup.class)  @RequestBody HiasEventClerkBo bo){
        return iHiasEventService.join(bo);
    }

    /**
     * @intro 查询活动申请列表接口
     * @param eventId 活动的id
     * @return 活动报名保存的情况
     */
    @GetMapping("/applylist")
    public List<HiasEventClerk> applyList(Long eventId){
        return iHiasEventService.applylist(eventId);
    }

    /**
     * @intro 审批活动加入接口
     * @param id 活动申请记录的ID
     * @param checkStaus 审批状态
     * @return 审批结果
     */
    @GetMapping("/check")
    public String checkApplication(Long id, int checkStaus){
        return iHiasEventService.checkApplication(id,checkStaus);
    }

//    //活动扩容
//    public String activeExpansion(Long id, int add){
//        return iHiasEventService.activeExpansion(id,add);
//    }
//
//    //关注活动
//    public String follow(HiasEventClerkBo bo){
//        return iHiasEventService.follow(bo);
//    }

    @GetMapping("/checkError")
    public R<Map<Long, String>> checkError() {
        return R.ok(iHiasEventService.checkError());
    }

    @GetMapping("/load")
    public Boolean load (){
        return iHiasEventService.load();
    }
    @GetMapping("/hot")
    public List<HiasEventHotVo> hot (Long clerkId, Long eventId){
        return iHiasEventService.hot(clerkId,eventId);
    }

    @PostMapping("/eventDelay")
    public R eventDelay(@RequestBody HiasEventDelayBo bo){
        return iHiasEventService.eventDelay(bo);
    }

}
