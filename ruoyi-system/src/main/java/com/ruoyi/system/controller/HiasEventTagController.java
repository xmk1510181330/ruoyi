package com.ruoyi.system.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.HiasEventTagVo;
import com.ruoyi.system.domain.bo.HiasEventTagBo;
import com.ruoyi.system.service.IHiasEventTagService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/eventTag")
public class HiasEventTagController extends BaseController {

    private final IHiasEventTagService iHiasEventTagService;

    /**
     * 查询【请填写功能名称】列表
     */
    @SaCheckPermission("system:eventTag:list")
    @GetMapping("/list")
    public TableDataInfo<HiasEventTagVo> list(HiasEventTagBo bo, PageQuery pageQuery) {
        return iHiasEventTagService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @SaCheckPermission("system:eventTag:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HiasEventTagBo bo, HttpServletResponse response) {
        List<HiasEventTagVo> list = iHiasEventTagService.queryList(bo);
        ExcelUtil.exportExcel(list, "【请填写功能名称】", HiasEventTagVo.class, response);
    }

    /**
     * 获取【请填写功能名称】详细信息
     *
     * @param eventId 主键
     */
    @SaCheckPermission("system:eventTag:query")
    @GetMapping("/{eventId}")
    public R<HiasEventTagVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long eventId) {
        return R.ok(iHiasEventTagService.queryById(eventId));
    }

    /**
     * 新增【请填写功能名称】
     */
    @SaCheckPermission("system:eventTag:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody HiasEventTagBo bo) {
        return toAjax(iHiasEventTagService.insertByBo(bo));
    }

    /**
     * 修改【请填写功能名称】
     */
    @SaCheckPermission("system:eventTag:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody HiasEventTagBo bo) {
        return toAjax(iHiasEventTagService.updateByBo(bo));
    }

    /**
     * 删除【请填写功能名称】
     *
     * @param eventIds 主键串
     */
    @SaCheckPermission("system:eventTag:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{eventIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] eventIds) {
        return toAjax(iHiasEventTagService.deleteWithValidByIds(Arrays.asList(eventIds), true));
    }
}
