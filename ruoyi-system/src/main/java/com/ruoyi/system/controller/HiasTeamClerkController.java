package com.ruoyi.system.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.HiasTeamClerkVo;
import com.ruoyi.system.domain.bo.HiasTeamClerkBo;
import com.ruoyi.system.service.IHiasTeamClerkService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/teamClerk")
public class HiasTeamClerkController extends BaseController {

    private final IHiasTeamClerkService iHiasTeamClerkService;

    /**
     * 查询【请填写功能名称】列表
     */
    @SaCheckPermission("system:teamClerk:list")
    @GetMapping("/list")
    public TableDataInfo<HiasTeamClerkVo> list(HiasTeamClerkBo bo, PageQuery pageQuery) {
        return iHiasTeamClerkService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @SaCheckPermission("system:teamClerk:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HiasTeamClerkBo bo, HttpServletResponse response) {
        List<HiasTeamClerkVo> list = iHiasTeamClerkService.queryList(bo);
        ExcelUtil.exportExcel(list, "【请填写功能名称】", HiasTeamClerkVo.class, response);
    }

    /**
     * 获取【请填写功能名称】详细信息
     *
     * @param teamId 主键
     */
    @SaCheckPermission("system:teamClerk:query")
    @GetMapping("/{teamId}")
    public R<HiasTeamClerkVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long teamId) {
        return R.ok(iHiasTeamClerkService.queryById(teamId));
    }

    /**
     * 新增【请填写功能名称】
     */
    @SaCheckPermission("system:teamClerk:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody HiasTeamClerkBo bo) {
        return toAjax(iHiasTeamClerkService.insertByBo(bo));
    }

    /**
     * 修改【请填写功能名称】
     */
    @SaCheckPermission("system:teamClerk:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody HiasTeamClerkBo bo) {
        return toAjax(iHiasTeamClerkService.updateByBo(bo));
    }

    /**
     * 删除【请填写功能名称】
     *
     * @param teamIds 主键串
     */
    @SaCheckPermission("system:teamClerk:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{teamIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] teamIds) {
        return toAjax(iHiasTeamClerkService.deleteWithValidByIds(Arrays.asList(teamIds), true));
    }
}
