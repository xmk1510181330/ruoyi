package com.ruoyi.system.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import com.ruoyi.system.domain.bo.*;
import com.ruoyi.system.domain.to.HiasActivationSafeTo;
import com.ruoyi.system.domain.vo.HiasActivationRegisterVo;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.HiasActivationVo;
import com.ruoyi.system.service.IHiasActivationService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】
 *
 * @author ruoyi
 * @date 2024-03-09
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/activation")
public class HiasActivationController extends BaseController {

    private final IHiasActivationService iHiasActivationService;

    /**
     * 查询【请填写功能名称】列表
     */
    @SaCheckPermission("system:activation:list")
    @GetMapping("/list")
    public TableDataInfo<HiasActivationVo> list(HiasActivationBo bo, PageQuery pageQuery) {
        return iHiasActivationService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @SaCheckPermission("system:activation:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HiasActivationBo bo, HttpServletResponse response) {
        List<HiasActivationVo> list = iHiasActivationService.queryList(bo);
        ExcelUtil.exportExcel(list, "【请填写功能名称】", HiasActivationVo.class, response);
    }

    /**
     * 获取【请填写功能名称】详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:activation:query")
    @GetMapping("/{id}")
    public R<HiasActivationVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iHiasActivationService.queryById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @SaCheckPermission("system:activation:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody HiasActivationBo bo) {
        return toAjax(iHiasActivationService.insertByBo(bo));
    }

    /**
     * 修改【请填写功能名称】
     */
    @SaCheckPermission("system:activation:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody HiasActivationBo bo) {
        return toAjax(iHiasActivationService.updateByBo(bo));
    }

    /**
     * 删除【请填写功能名称】
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:activation:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iHiasActivationService.deleteWithValidByIds(Arrays.asList(ids), true));
    }


    @GetMapping("/generateSafe")
    public HiasActivationSafeTo generateDeviceIdSafe(@RequestBody HiasActivationSafeBo safeBo) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        return iHiasActivationService.generateDeviceIdSafe(safeBo);
    }

    @PostMapping("/generateCode")
    public R generateActivationCode(@RequestBody HiasActivationGenBo bo){
        return iHiasActivationService.generateActivationCode(bo);
    }

    @PostMapping("/generateCodeSafe")
    public R generateActivationCodeSafe(@RequestBody HiasActivationSafeBo safeBo) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        return iHiasActivationService.generateActivationCodeSafe(safeBo);
    }

    @PostMapping("/check")
    public R check(@RequestBody HiasActivationCheckBo bo){
        return iHiasActivationService.check(bo);
    }

    @PostMapping("/checkSafe")
    public R checkSafe(@RequestBody HiasActivationSafeBo safeBo) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        return iHiasActivationService.checkSafe(safeBo);
    }

}
