package com.ruoyi.system.controller;

import java.util.Date;
import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import com.ruoyi.system.domain.HiasEnterprise;
import com.ruoyi.system.domain.HiasInnovationPark;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.HiasInnovationParkVo;
import com.ruoyi.system.domain.bo.HiasInnovationParkBo;
import com.ruoyi.system.service.IHiasInnovationParkService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/innovationPark")
public class HiasInnovationParkController extends BaseController {

    private final IHiasInnovationParkService iHiasInnovationParkService;

    /**
     * 查询【请填写功能名称】列表
     */
    @SaCheckPermission("system:innovationPark:list")
    @GetMapping("/list")
    public TableDataInfo<HiasInnovationParkVo> list(HiasInnovationParkBo bo, PageQuery pageQuery) {
        return iHiasInnovationParkService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @SaCheckPermission("system:innovationPark:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HiasInnovationParkBo bo, HttpServletResponse response) {
        List<HiasInnovationParkVo> list = iHiasInnovationParkService.queryList(bo);
        ExcelUtil.exportExcel(list, "【请填写功能名称】", HiasInnovationParkVo.class, response);
    }

    /**
     * 获取【请填写功能名称】详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:innovationPark:query")
    @GetMapping("/{id}")
    public R<HiasInnovationParkVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iHiasInnovationParkService.queryById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @SaCheckPermission("system:innovationPark:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody HiasInnovationParkBo bo) {
        return toAjax(iHiasInnovationParkService.insertByBo(bo));
    }

    /**
     * 修改【请填写功能名称】
     */
    @SaCheckPermission("system:innovationPark:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody HiasInnovationParkBo bo) {
        return toAjax(iHiasInnovationParkService.updateByBo(bo));
    }

    /**
     * 删除【请填写功能名称】
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:innovationPark:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iHiasInnovationParkService.deleteWithValidByIds(Arrays.asList(ids), true));
    }





    //修改园区信息

    //分页查询园区信息

    //查询园区详细信息

    //删除园区信息

    //查询园区入驻企业
    @GetMapping("/selectEnterpriseByParkId/{parkId}")
    public List<HiasEnterprise> selectEnterpriseByParkId(Long parkId){
        return iHiasInnovationParkService.selectEnterpriseByParkId(parkId);
    }






    //企业入驻园区






}
