package com.ruoyi.system.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import com.ruoyi.system.domain.HiasEnterprise;
import com.ruoyi.system.domain.vo.HiasEnterpriseDetailVo;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.HiasEnterpriseVo;
import com.ruoyi.system.domain.bo.HiasEnterpriseBo;
import com.ruoyi.system.service.IHiasEnterpriseService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/enterprise")
public class HiasEnterpriseController extends BaseController {

    private final IHiasEnterpriseService iHiasEnterpriseService;


    /**
     * @intro 根据企业名称查询企业信息（不是详细信息）
     * @param name 企业名称
     * @return 企业信息列表
     */
    @GetMapping("/selectByName")
    public List<HiasEnterprise> selectByName (String name){
        return iHiasEnterpriseService.selectByName(name);
    }

    /**
     * @intro 根据分类标签查找企业
     * @param cid 企业的分类标签
     * @return 企业信息列表
     */
    @GetMapping("/selectByCategory")
    public List<HiasEnterprise> selectByCategory(int cid){
        return iHiasEnterpriseService.selectByCategory(cid);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @SaCheckPermission("system:enterprise:list")
    @GetMapping("/list")
    public TableDataInfo<HiasEnterpriseVo> list(HiasEnterpriseBo bo, PageQuery pageQuery) {
        return iHiasEnterpriseService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @SaCheckPermission("system:enterprise:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HiasEnterpriseBo bo, HttpServletResponse response) {
        List<HiasEnterpriseVo> list = iHiasEnterpriseService.queryList(bo);
        ExcelUtil.exportExcel(list, "【请填写功能名称】", HiasEnterpriseVo.class, response);
    }

    /**
     * @intro 查询企业的详细信息（包括科研设备、科研成果）？
     * @param id 主键
     * @return 企业详细信息的另一个vo
     */
    @SaCheckPermission("system:enterprise:query")
    @GetMapping("/{id}")
    public R<HiasEnterpriseDetailVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iHiasEnterpriseService.queryById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @SaCheckPermission("system:enterprise:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody HiasEnterpriseBo bo) {
        return toAjax(iHiasEnterpriseService.insertByBo(bo));
    }

    /**
     * 修改【请填写功能名称】
     */
    @SaCheckPermission("system:enterprise:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody HiasEnterpriseBo bo) {
        return toAjax(iHiasEnterpriseService.updateByBo(bo));
    }

    /**
     * 删除【请填写功能名称】
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:enterprise:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iHiasEnterpriseService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
