package com.ruoyi.system.controller;

import java.util.List;
import java.util.Arrays;

import com.dtflys.forest.annotation.Get;
import com.ruoyi.system.domain.bo.HiasTeamClerkBo;
import com.ruoyi.system.domain.vo.HiasTeamApplyRecordVo;
import com.ruoyi.system.domain.vo.HiasTeamApplyVo;
import com.ruoyi.system.domain.vo.HiasTeamRecordVo;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.HiasTeamVo;
import com.ruoyi.system.domain.bo.HiasTeamBo;
import com.ruoyi.system.service.IHiasTeamService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/team")
public class HiasTeamController extends BaseController {

    private final IHiasTeamService iHiasTeamService;

    /**
     * 查询【请填写功能名称】列表
     */
    @SaCheckPermission("system:team:list")
    @GetMapping("/list")
    public TableDataInfo<HiasTeamVo> list(HiasTeamBo bo, PageQuery pageQuery) {
        return iHiasTeamService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @SaCheckPermission("system:team:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HiasTeamBo bo, HttpServletResponse response) {
        List<HiasTeamVo> list = iHiasTeamService.queryList(bo);
        ExcelUtil.exportExcel(list, "【请填写功能名称】", HiasTeamVo.class, response);
    }

    /**
     * 获取【请填写功能名称】详细信息
     *
     * @param tName 主键
     */
    @SaCheckPermission("system:team:query")
    @GetMapping("/{teamId}")
    public R<HiasTeamVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long teamId) {
        return R.ok(iHiasTeamService.queryById(teamId));
    }

    /**
     * 新增【请填写功能名称】
     */
    @SaCheckPermission("system:team:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody HiasTeamBo bo) {
        return toAjax(iHiasTeamService.insertByBo(bo));
    }

    /**
     * 修改【请填写功能名称】
     */
    @SaCheckPermission("system:team:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody HiasTeamBo bo) {
        return toAjax(iHiasTeamService.updateByBo(bo));
    }

    /**
     * 删除【请填写功能名称】
     *
     * @param tNames 主键串
     */
    @SaCheckPermission("system:team:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{tNames}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable String[] tNames) {
        return toAjax(iHiasTeamService.deleteWithValidByIds(Arrays.asList(tNames), true));
    }
    @PostMapping("/join")
    public R join(@RequestBody HiasTeamClerkBo hiasTeamClerkBo){
        boolean result = iHiasTeamService.join(hiasTeamClerkBo);
        if(result){
            return R.ok("提交申请成功，请等待审批");
        }else {
            return R.fail("系统繁忙，请稍后重试");
        }
    }

    /**
     * @info 获取团队报名列表 -- 团队管理员
     * @param teamId 团队ID
     * @return 团队申请列表信息
     */
    @GetMapping("applyList")
    public R<HiasTeamApplyVo> applyList(Long teamId) {
        HiasTeamApplyVo vo = iHiasTeamService.applyList(teamId);
        return R.ok(vo);
    }

    /**
     * @info 获取团队报名列表 -- 用户
     * @param clerkId 用户ID
     * @return 用户的报名记录
     */
    @GetMapping("applyRecords")
    public R<HiasTeamRecordVo> applyRecords(Long clerkId) {
        HiasTeamRecordVo vo = iHiasTeamService.applyRecords(clerkId);
        return R.ok(vo);
    }

    /**
     * @info 审批加入团队的申请
     * @param teamClerkBo 审批BO
     * @return 审批结果
     */
    @PostMapping("checkApply")
    public R checkApply(@RequestBody HiasTeamClerkBo teamClerkBo) {
        boolean result = iHiasTeamService.checkApply(teamClerkBo);
        if(result) {
            return R.ok("审批完毕");
        }else {
            return R.fail("系统内部错误");
        }
    }

}
