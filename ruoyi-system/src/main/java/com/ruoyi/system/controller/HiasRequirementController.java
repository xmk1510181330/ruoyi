package com.ruoyi.system.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.HiasRequirementVo;
import com.ruoyi.system.domain.bo.HiasRequirementBo;
import com.ruoyi.system.service.IHiasRequirementService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/requirement")
public class HiasRequirementController extends BaseController {

    private final IHiasRequirementService iHiasRequirementService;

    /**
     * 查询【请填写功能名称】列表
     */
    @SaCheckPermission("system:requirement:list")
    @GetMapping("/list")
    public TableDataInfo<HiasRequirementVo> list(HiasRequirementBo bo, PageQuery pageQuery) {
        return iHiasRequirementService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @SaCheckPermission("system:requirement:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HiasRequirementBo bo, HttpServletResponse response) {
        List<HiasRequirementVo> list = iHiasRequirementService.queryList(bo);
        ExcelUtil.exportExcel(list, "【请填写功能名称】", HiasRequirementVo.class, response);
    }

    /**
     * 获取【请填写功能名称】详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:requirement:query")
    @GetMapping("/{id}")
    public R<HiasRequirementVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iHiasRequirementService.queryById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @SaCheckPermission("system:requirement:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody HiasRequirementBo bo) {
        return toAjax(iHiasRequirementService.insertByBo(bo));
    }

    /**
     * 修改【请填写功能名称】
     */
    @SaCheckPermission("system:requirement:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody HiasRequirementBo bo) {
        return toAjax(iHiasRequirementService.updateByBo(bo));
    }

    /**
     * 删除【请填写功能名称】
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:requirement:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iHiasRequirementService.deleteWithValidByIds(Arrays.asList(ids), true));
    }

    //申请需求合作
    @PostMapping("apply")
    public R apply(Long did, Long cid){
        boolean result = iHiasRequirementService.apply(did, cid);
        if (result) {
            return R.ok("合作意向已经提交，请稍后等待联络");
        }
        else {
            return R.fail("资质不足，请寻找小额合作");
        }
    }



}
