package com.ruoyi.system.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.HiasCategoryVo;
import com.ruoyi.system.domain.bo.HiasCategoryBo;
import com.ruoyi.system.service.IHiasCategoryService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/category")
public class HiasCategoryController extends BaseController {

    private final IHiasCategoryService iHiasCategoryService;

    /**
     * 查询【请填写功能名称】列表
     */
    @SaCheckPermission("system:category:list")
    @GetMapping("/list")
    public TableDataInfo<HiasCategoryVo> list(HiasCategoryBo bo, PageQuery pageQuery) {
        return iHiasCategoryService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @SaCheckPermission("system:category:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HiasCategoryBo bo, HttpServletResponse response) {
        List<HiasCategoryVo> list = iHiasCategoryService.queryList(bo);
        ExcelUtil.exportExcel(list, "【请填写功能名称】", HiasCategoryVo.class, response);
    }

    /**
     * 获取【请填写功能名称】详细信息
     *
     * @param name 主键
     */
    @SaCheckPermission("system:category:query")
    @GetMapping("/{name}")
    public R<HiasCategoryVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable String name) {
        return R.ok(iHiasCategoryService.queryById(name));
    }

    /**
     * 新增【请填写功能名称】
     */
    @SaCheckPermission("system:category:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody HiasCategoryBo bo) {
        return toAjax(iHiasCategoryService.insertByBo(bo));
    }

    /**
     * 修改【请填写功能名称】
     */
    @SaCheckPermission("system:category:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody HiasCategoryBo bo) {
        return toAjax(iHiasCategoryService.updateByBo(bo));
    }

    /**
     * 删除【请填写功能名称】
     *
     * @param names 主键串
     */
    @SaCheckPermission("system:category:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{names}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable String[] names) {
        return toAjax(iHiasCategoryService.deleteWithValidByIds(Arrays.asList(names), true));
    }
}
